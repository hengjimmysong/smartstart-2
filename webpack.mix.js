const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/admin/admin.scss', 'public/css/admin.css')
   .sass('resources/sass/client/client.scss', 'public/css/client.css')
   .sass('resources/sass/client/main-page/m-pages.scss', 'public/css/m-pages.css')
   .sass('resources/sass/client/hse/hse-pages.scss', 'public/css/hse-pages.css')
   .sass('resources/sass/client/yip/yip-pages.scss', 'public/css/yip-pages.css')
   .sass('resources/sass/client/ulp/ulp-pages.scss', 'public/css/ulp-pages.css');
