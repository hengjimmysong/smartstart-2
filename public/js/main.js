window.onload = function () {
  showCycle('cycle1')
  moveSlideshow(0);
  setTimeout(() => {
    moveSlideshow(1);
  }, 5000);
}

function showCycle(id) {
  let cycles = Array.from(document.getElementsByTagName('cycle'));

  cycles.forEach((el) => {
    el.id == id || el.id == `${id}-kh` ? el.style.display = 'flex' : el.style.display = 'none';
  });
  if (document.getElementById('btn-cycle1')) {
    document.getElementById('btn-cycle1').classList.remove('active');
  }
  if (document.getElementById('btn-cycle1-kh')) {
    document.getElementById('btn-cycle1-kh').classList.remove('active');
  }
  if (document.getElementById('btn-cycle2')) {
    document.getElementById('btn-cycle2').classList.remove('active');
  }
  if (document.getElementById('btn-cycle2-kh')) {
    document.getElementById('btn-cycle2-kh').classList.remove('active');
  }
  if (document.getElementById(`btn-${id}`)) {
    document.getElementById(`btn-${id}`).classList.add('active');
  }
  if (document.getElementById(`btn-${id}-kh`)) {
    document.getElementById(`btn-${id}-kh`).classList.add('active');
  }
}

function openCard(id) {
  let cards = Array.from(document.getElementsByTagName('card'));

  cards.forEach((el) => {
    el.id == id || el.id == `${id}-kh` ? el.style.display = 'flex' : el.style.display = 'none';
  });
  if (document.getElementById('cards')) {
    document.getElementById('cards').style.display = 'flex';
  }
  if (document.getElementById('cards-kh')) {
    document.getElementById('cards-kh').style.display = 'flex';
  }
}

function closeCard() {
  if (document.getElementById('cards')) {
    document.getElementById('cards').style.display = 'none';
  }

  if (document.getElementById('cards-kh')) {
    document.getElementById('cards-kh').style.display = 'none';
  }
}

let maxSlide = 4;
let currentSlide = 0;

function moveSlideshow(direction) {
  currentSlide += direction;

  currentSlide >= maxSlide ? currentSlide = 0 : null;

  if (document.getElementById('slideshow')) {
    document.getElementById('slideshow').style.transform = `translateX(${-currentSlide * 100}vw)`;
  }
  if (document.getElementById('slideshow-kh')) {
    document.getElementById('slideshow-kh').style.transform = `translateX(${-currentSlide * 100}vw)`;
  }
  if (currentSlide <= 0) {
    if (document.getElementById('l-arrow')) {
      document.getElementById('l-arrow').style.display = 'none';
    }
    if (document.getElementById('l-arrow-kh')) {
      document.getElementById('l-arrow-kh').style.display = 'none';
    }
  } else {
    if (document.getElementById('l-arrow')) {
      document.getElementById('l-arrow').style.display = 'unset';
    }
    if (document.getElementById('l-arrow-kh')) {
      document.getElementById('l-arrow-kh').style.display = 'unset';
    }
  }

  if (currentSlide >= maxSlide || currentSlide < 0) {
    if (document.getElementById('r-arrow')) {
      document.getElementById('r-arrow').style.display = 'none';
    }
    if (document.getElementById('r-arrow-kh')) {
      document.getElementById('r-arrow-kh').style.display = 'none';
    }
  } else {
    if (document.getElementById('r-arrow')) {
      document.getElementById('r-arrow').style.display = 'unset';
    }
    if (document.getElementById('r-arrow-kh')) {
      document.getElementById('r-arrow-kh').style.display = 'unset';
    }
  }

  if (currentSlide >= 0) {
    if (document.getElementById('index')) {
      document.getElementById('index').style.opacity = 1;
    }
    if (document.getElementById('index-kh')) {
      document.getElementById('index-kh').style.opacity = 1;
    }
    let dots = Array.from(document.getElementsByTagName('dot'));
    dots.forEach((el) => {
      el.id == currentSlide ? el.classList.add('active') : el.classList.remove('active');
    })
  } else {
    if (document.getElementById('index')) {
      document.getElementById('index').style.opacity = 0;
    }
    if (document.getElementById('index-kh')) {
      document.getElementById('index-kh').style.opacity = 0;
    }
  }
}
