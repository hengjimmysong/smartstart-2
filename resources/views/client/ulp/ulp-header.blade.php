<div class="header ulp-header" data-scroll-pos="0">
  <div class="logo" style="background-image: url('/images/ulp/ulp.png')" onclick="customScrollTo('top')"></div>
  <div id="menu" class="menu ulp">
    <div id="h-home" class="btn item" onclick="customScrollTo('top')" data-scrolled-passed="true">Home</div>
    <div id="h-about" class="btn item" onclick="customScrollTo('about')">About Us</div>
    <div id="h-objective" class="btn item" onclick="customScrollTo('objective')">Objective</div>
    <div id="h-testimonials" class="btn item" onclick="customScrollTo('testimonials')">Testimonials</div>
    <div class="btn item highlight" onclick="window.open('https://ulp.smartstart.com.kh/sign-in')">ULP Site</div>
    <div id="h-lang" class="btn item lang" onclick="window.location.href = '/kh/ulp'"
      style="background-image: url('/images/client/eng.png')"></div>
  </div>
  <div class="menu-btn" onclick="toggleNavBar()"></div>
</div>
