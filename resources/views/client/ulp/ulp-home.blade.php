@extends('layouts.ulp.ulp-layout')
@section('header')
<title>SmartStart - ULP</title>
@endsection

@section('content')
<div class="floating-btn left" onclick="window.location.href = '/'">
  <p class="label">Back to Home</p>
  <div class="icon" style="background-image: url('/images/client/left-white.png')"></div>
</div>

<div class="floating-btn right" onclick="customScrollTo('top')">
  <div class="icon" style="background-image: url('/images/client/up-white.png')"></div>
  <p class="label">Scroll to Top</p>
</div>

<div class="ulp-pages ulp-home">
  <div class="section cover"
    style="background:   linear-gradient( rgba(0, 154, 62, 1),rgba(0, 154, 62, 1),rgba(0, 154, 62, 1),rgba(0, 154, 62, 1), rgba(0, 154, 62, 0.2))">
    <div class="background"></div>
    <div class="ctn">
      {{-- <p class="title">Welcome to</p> --}}
      <div class="logo" style="background-image: url('/images/client/logo-white.png')"></div>
      <p class="subtitle">Unipreneur Learning Platform</p>
      <div class="divider"></div>
      <p class="text">
        The SmartStart Unipreneur Learning Platform is an educational initiative between Smart Axiata and Impact Hub
        Phnom Penh. It is the first local online learning and interactive platform that introduces entrepreneurship to
        Cambodian students (in Khmer and English) through partnerships with universities and a blended learning
        approach.
        <br><br>
        The goal of the learning platform and course, designed and built in Cambodia, is to develop the technical
        capabilities and entrepreneurial mindsets of students through the practical implementation of related concepts.
        <br><br>
        This initiative has been made possible thanks to Smart’s contribution to the Capacity Building and R&D Fund and
        their strong partnership with the Ministry of Posts and Telecommunications.

      </p>
    </div>
  </div>

  <div id="about" class="section border about">
    <div class="title-line"></div>
    <p class="section-title">About us</p>
    <div class="ctn">
      <div class="image methodology" style="background-image: url('/images/ulp/methodology.svg')">

      </div>
      <div class="info">
        <div class="title">Methodology</div>
        <p class="text">
          SmartStart Unipreneur Learning Platform is a blended learning approach to Cambodian university students in
          Khmer and English.
          <br><br>
          This Learning Management System (LMS) delivers a blended (on and offline) entrepreneurship program by
          providing university students the latest content that is a mixture of global startup theories and successful
          Cambodian examples.
          <br><br>
          In order to ensure a high level of students’ understanding, courses embed written text, videos, quizzes and
          practical activities for a more engaging, blended learning user experience for university students. For
          example, online activities require students to build canvases as well as interact and work together whereas
          offline activities require students to undertake practical work outside the classroom to ‘learn by doing’.
        </p>
      </div>
    </div>

    <div class="ctn reverse">
      <div class="image coursesyllabus" style="background-image: url('/images/ulp/coursesyllabus.svg')">

      </div>
      <div class="info">
        <div class="title">Course Syllabus</div>
        <p class="text">
          1. During the ‘Design Thinking’ phase students create their innovative ideas. They first spend time
          interviewing
          ‘customers’ in order to understand their problems and build empathy for them. They then define the customers
          problem to be solved from the customers’ perspective. Ideas are generated with the goal of being new and
          relevant for the needs of the customer.
          <br>
          <br>
          2. Students ‘learn by doing’ through the ‘Lean Startup’ phase. They analyse their idea to identify key
          assumptions. They then create prototypes of their product/service to test test assumptions. Prototypes are
          tested with ‘customers’ for their feedback. Students are encouraged to learn from ‘failure’ and use this
          information gained to make improvements. Students gain skills in critical analysis and build the confidence to
          be proactive and take risks.
          <br>
          <br>
          3. Students learn the key functions of a business model through the ‘BMC’ phase. They learn how to create a
          viable and sustainable business model through the 9 BMC blocks. The course ends with a pitch to the class to
          present their product/service and BMC.
          <br><br>
          The course in numbers:
        <ul>
          <li>15 week course</li>
          <li>45 hours of work</li>
          <li>30 hours classroom based, 15 hours outside classrooms</li>
        </ul>
        </p>
      </div>
    </div>
  </div>

  <div id="objective" class="section border objective">
    <div class="title-line"></div>
    <p class="section-title">Objective</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/ulp/objective.svg')">
      </div>
      <div class="info">
        <p class="text">
          By going through the courses, university students will gain an in-depth understanding of entrepreneurship,
          develop their entrepreneurial mindset, and improve their employability and life skills, so they can be exposed
          to the local market and its opportunities for their future careers, and be inspired by local business leaders.
          <br><br>
          This platform is relevant and valuable for all university students - even if they do not want/cannot start
          their own businesses - since having an entrepreneurial mindset is a valuable life skill for anyone’s personal
          and professional development.
          <br><br>
          Ultimately, the platform’s objective is to equip young people with the skills, knowledge and motivation to
          become valuable members of the community when they graduate.
        </p>
      </div>
    </div>
  </div>

  <div id="testimonials" class="section border testimonials">
    <div class="title-line"></div>
    <p class="section-title">Testimonials</p>
    <div class="ctn">
      <div class="image-ctn">

        <div class="border-ctn">
          <div class="image" style="background-image: url('/images/ulp/t1.jpeg')"></div>
        </div>
      </div>

      <div class="info">
        <p class="name">Nging <span>Muy</span></p>
        <p class="text">
          “I learnt the differences between business and
          entrepreneurship. I love this course the most compared to others. I can not imagine before, but this course
          have allowed me to increase my awareness about problems and opportunities and how we can be part of the
          solution. Now I realized how big the world is; as well as the problems.”
        </p>
      </div>
    </div>

    <div class="ctn reverse">
      <div class="image-ctn">

        <div class="border-ctn">
          <div class="image" style="background-image: url('/images/ulp/t2.jpeg')"></div>
        </div>
      </div>

      <div class="info">
        <p class="name">Chamroeun <span>Yorngsok</span></p>
        <p class="text">
          “Sarath (lecturer for RULE) said we don’t just
          need to have an entrepreneurial mindset for
          doing business, but actually it is a part of our
          life and it will lead us to do well in many
          things. I really love this part- it has changed my
          idea towards having an entrepreneurial
          mindset.”
        </p>
      </div>
    </div>
    
  </div>

  <div id='partner' class="section border partner">
    <div class="title-line"></div>
    <p class="section-title">University Partners</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/ulp/partneruni.svg')">
      </div>
      <div class="partners">
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u1.png')"></div>
          <div class="text">
            <p>Cambodia Academy of Digital Technology</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u2.png')"></div>
          <div class="text">
            <p>University of Puthisastra</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u3.png')"></div>
          <div class="text s">
            <p>National University of Management</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u4.png')"></div>
          <div class="text s">
            <p>Royal University of Law and Economics</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u5.png')"></div>
          <div class="text s">
            <p>Build Bright University Phnom Penh</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u5.png')"></div>
          <div class="text s">
            <p>Build Bright University Siem Reap</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u6.png')"></div>
          <div class="text">
            <p>Royal University of Agriculture</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u7.png')"></div>
          <div class="text">
            <p>Cambodia University of Technology and Science</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u8.png')"></div>
          <div class="text s">
            <p>Paññāsāstra University of Cambodia Battambang</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u9.png')"></div>
          <div class="text">
            <p>Angkor University</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u10.png')"></div>
          <div class="text">
            <p>Svay Rieng University</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section partnership">
    <div class="title-line"></div>
    <p class="section-title">In Partnership With</p>
    <div class="ctn">
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/client/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/client/CBRD.png')"></div>
      </div>
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/Impact.png')"></div>
      </div>
      <div class="item">
        <div class="logo smart" style="background-image: url('/images/client/Smart.png')"></div>
      </div>
    </div>

    {{-- <div class="tagline-ctn">
      <div class="tagline">Innovating With You, Investing In You!</div>
      <div class="logo" style="background-image: url('/images/yip/yip.png')"></div>
    </div> --}}
  </div>
</div>
@endsection
