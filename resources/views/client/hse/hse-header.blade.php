<div class="header hse-header" data-scroll-pos="0">
  <div class="logo" style="background-image: url('/images/hse/hse.png')" onclick="customScrollTo('top')"></div>
  <div id="menu" class="menu">
    <div id="h-home" class="btn item" onclick="customScrollTo('top')" data-scrolled-passed="true">Overview</div>
    <div id="h-objectives" class="btn item" onclick="customScrollTo('objectives')">Objectives</div>
    <div id="h-structure" class="btn item" onclick="customScrollTo('structure')">Structure</div>
    <div id="h-benefits" class="btn item" onclick="customScrollTo('benefits')">Benefits</div>
    <div id="h-criteria" class="btn item" onclick="customScrollTo('criteria')">Criteria</div>
    <div id="h-lang" class="btn item lang" onclick="window.location.href = '/kh/hse'"
      style="background-image: url('/images/client/eng.png')"></div>
  </div>
  <div class="menu-btn" onclick="toggleNavBar()"></div>
</div>
