@extends('layouts.hse.hse-layout')
@section('header')
<title>SmartStart - HSE</title>
@endsection

@section('content')
<div class="floating-btn left" onclick="window.location.href = '/'">
  <p class="label">Back to Home</p>
  <div class="icon" style="background-image: url('/images/client/left-white.png')"></div>
</div>

<div class="floating-btn right" onclick="customScrollTo('top')">
  <div class="icon" style="background-image: url('/images/client/up-white.png')"></div>
  <p class="label">Scroll to Top</p>
</div>

<div class="hse-pages hse-home">
  <div class="section cover"
    style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('/images/client/p-4.jpg')">
    <p class="title">
      SmartStart
    </p>
    <div class="description">High School Edition</div>
    <div class="btn" style="background-image: url('/images/client/sim-cut-green.png')"
      onclick="window.open('https://bit.ly/3tmcAMO')">
      <p class="text">Apply Now</p>
      <div class="icon" style="background-image: url('/images/client/right-white.png')"></div>
    </div>
  </div>
  <div class="section border about-us">
    <p class="subtitle">Overview</p>
    <div class="title-line"></div>
    <p class="section-title">About SmartStart High School Edition</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/hse/about-us.png')">

      </div>
      <div class="info">
        <p class="text">
          Building on the success of the flagship SmartStart Young Innovator Program, the newly launched SmartStart High
          School Edition is a 2.5-day program that will provide exposure to digital business and entrepreneurship for
          high school students.
          <br><br>
          SmartStart High School Edition is designed to introduce young high
          school students to the fundamentals of digital entrepreneurship by
          balancing digital business sessions, entrepreneurship and crucial soft
          skills, and to cultivate an entrepreneurial mindset amongst students.
          <br><br>
          Participants are expected to gain a broad understanding of business skills and tools through structured
          workshops, simulations, group dynamics, coaching as well as case studies throughout the whole program.
        </p>
      </div>
    </div>
  </div>
  <div id="objectives" class="section border stake">
    <p class="subtitle">Objectives</p>
    <div class="title-line"></div>
    <p class="section-title">What are the expected outcomes?</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/hse/objective.png')">

      </div>
      <div class="info">
        {{-- <div class="title">Program Structure</div> --}}
        <p class="text">
          The curriculum comprises three areas to ensure holistic
          development for participants. In keeping with the digital
          innovations of our age, the program will introduce
          participants to current and future technological trends.
          Combined with a basic understanding of technology,
          participants will also get exposed to the fundamental
          concepts of entrepreneurship and how an entrepreneurial
          mindset can be useful in leading a fulfilling lifestyle.
          Throughout the program, participants will get to learn
          essential soft skills that will boost their self-development
          and employability in the future.
        </p>
        <ul>
          <li>Provide basic knowledge of business plans and business canvases</li>
          <li>Expose students to observe their community and turn problems into business opportunities</li>
          <li>Gain soft skills including communication, teamwork and public speaking</li>
          <li>Get inspired and insight into an entrepreneurial journey</li>
          <li>Learn by doing through immediate execution</li>
          <li>Receive feedback from dedicated mentors</li>
        </ul>
      </div>
    </div>
  </div>
  <div id="structure" class="section border structure">
    <p class="subtitle">Structure</p>
    <div class="title-line"></div>
    <p class="section-title">What will you go through?</p>
    <div id="structure-ctn" class="ctn">
      <div class="item">
        <div class="image" style="background-image: url('/images/hse/rocket.png')"></div>
        <p class="title">Digital Business</p>
        {{-- <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div> --}}
      </div>

      <div class="item">
        <div class="image" style="background-image: url('/images/hse/brain.png')"></div>
        <p class="title">Entrepreneurship</p>
        {{-- <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div> --}}
      </div>

      <div class="item">
        <div class="image" style="background-image: url('/images/hse/light-bulb.png')"></div>
        <p class="title">Soft Skills</p>
        {{-- <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div> --}}
      </div>
    </div>
    <div class="slider-btns">
      <div class="btn left" onclick="structureSlider(-1)">
        <div class="icon"></div>
        <p class="label">Left</p>
      </div>
      <div class="btn right" onclick="structureSlider(1)">
        <p class="label">Right</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>
  <div id="benefits" class="section border benefits">
    <p class="subtitle">Benefits</p>
    <div class="title-line"></div>
    <p class="section-title">What Will You Gain?</p>
    <div class="ctn mobile" style="background-image: url('/images/hse/benefits-mobile.png')">
      <div class="item tl">Stand a chance to join a startup tour in Phnom Penh and visit startup offices.</div>
      <div class="item tr">Get a deep understanding of the fundamental concepts of digital business and
        entrepreneurship. Students will also develop crucial soft skills to prepare them for their future.</div>
      <div class="item bl">Meet inspiring entrepreneurs to learn more about their real-life entrepreneur experience.
      </div>
      <div class="item br">Stand a chance to win a grand prize totalling up to 2,200 USD.</div>
    </div>

    <div class="ctn web" style="background-image: url('/images/hse/benefits.png')">
      <div class="item tl">Stand a chance to join a startup tour in Phnom Penh and visit startup offices.</div>
      <div class="item tr">Get a deep understanding of the fundamental concepts of digital business and
        entrepreneurship. Students will also develop crucial soft skills to prepare them for their future.</div>
      <div class="item bl">Meet inspiring entrepreneurs to learn more about their real-life entrepreneur experience.
      </div>
      <div class="item br">Stand a chance to win a grand prize totalling up to 2,200 USD.</div>
    </div>
  </div>
  <div id="criteria" class="section border who-can-apply">
    <p class="subtitle">Criteria</p>
    <div class="title-line"></div>
    <p class="section-title">Who are we looking for?</p>
    <p class="small-section-title">At the minimum, candidates must be:</p>
    <div class="ctn">
      <div class="item">
        <div class="image" style="background-image: url('/images/hse/school.png')"></div>
        <div class="divider"></div>
        <p class="title">High School<br>10<sup>th</sup> - 11<sup>th</sup> Graders</p>
      </div>
      <div class="item">
        <div class="image" style="background-image: url('/images/hse/citizen.png')"></div>
        <div class="divider"></div>
        <p class="title">Cambodian citizens residing in <br>Kampong Cham, <br>Kampong Chnnang, <br>and Kampong Thom</p>
      </div>
      <div class="item">
        <div class="image" style="background-image: url('/images/hse/participation.png')"></div>
        <div class="divider"></div>
        <p class="title">Able to attend the 2.5 day<br> program in full</p>
      </div>
    </div>
  </div>

  {{-- <div class="section border faq">
    <div class="title-line"></div>
    <p class="section-title">Frequently Asked Questions</p>
    <div id="faq-ctn" class="ctn">
      <div class="item">
        <div class="image"></div>
        <p class="title">General Questions</p>
        <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div>
      </div>

      <div class="item">
        <div class="image"></div>
        <p class="title">Application Criteria</p>
        <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div>
      </div>

      <div class="item">
        <div class="image"></div>
        <p class="title">Self Cirriculum</p>
        <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div>
      </div>
    </div>
    <div class="slider-btns">
      <div class="btn left" onclick="faqSlider(-1)"  style="background-image: url('/images/client/left.png')">
      </div>
      <div class="btn right" onclick="faqSlider(1)"  style="background-image: url('/images/client/right.png')">
      </div>
    </div>
  </div> --}}

  <div class="section apply-now"
    style="background: linear-gradient( rgba(0, 154, 62, 1),rgba(0, 154, 62, 0), rgba(0, 154, 62, 0) ), url('/images/client/p-7.jpg')">
    <p class="title">Are You Ready To Innovate With Smart?</p>

    <div class="btn" style="background-image: url('/images/client/sim-cut-green.png')"
      onclick="window.open('https://bit.ly/3tmcAMO')">
      <p class="text">Apply Now</p>
      <div class="icon" style="background-image: url('/images/client/right-white.png')"></div>
    </div>
  </div>

  <div class="section partnership">
    <div class="title-line"></div>
    <p class="section-title">In Partnership With</p>
    <div class="ctn">
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/client/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/client/CBRD.png')"></div>
      </div>
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MEF.png')"></div>
        <div class="logo KE" style="background-image: url('/images/client/KE.png')"></div>
        <div class="logo smart" style="background-image: url('/images/client/Smart.png')"></div>
      </div>
    </div>

    <div class="tagline-ctn">
      <div class="tagline">Develop Young Entrepreneurs</div>
      {{-- <div class="logo" style="background-image: url('/images/hse/hse.png')"></div> --}}
    </div>
  </div>
</div>
@endsection
