<div class="footer">
  {{-- @if($page != '')
  <div class="page-identifier">
    {{ $page }}
  </div>
  @endif --}}
  <div class="ctn">
    <div class="item">
      <p class="text">Follow us on</p>
      <div class="row">
        <div class="icon" style="background-image: url('/images/client/fb.png')"
          onclick="window.open('https://www.facebook.com/smartforcambodia/')"></div>
        <div class="icon" style="background-image: url('/images/client/linkedin.png')"
          onclick="window.open('https://www.linkedin.com/company/smartaxiata/')"></div>
        <div class="icon yt" style="background-image: url('/images/client/yt.png')"
          onclick="window.open('https://www.youtube.com/user/SmartAxiata')"></div>
        <div class="icon" style="background-image: url('/images/client/ig.png')"
          onclick="window.open('https://www.instagram.com/smartaxiata/?hl=en')"></div>

      </div>
    </div>
  </div>
  <div class="smart-footer" style="background-image: url('/images/client/smart-footer.png')"></div>
  <p class="copyright">SmartStart - © 2021. All Rights Reserved. </p>
</div>
