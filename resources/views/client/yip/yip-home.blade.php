@extends('layouts.yip.yip-layout')
@section('header')
<title>SmartStart - YIP</title>
@endsection

@section('content')
<div class="floating-btn left" onclick="window.location.href = '/'">
  <p class="label">Back to Home</p>
  <div class="icon" style="background-image: url('/images/client/left-white.png')"></div>
</div>

<div class="floating-btn right" onclick="customScrollTo('top')">
  <div class="icon" style="background-image: url('/images/client/up-white.png')"></div>
  <p class="label">Scroll to Top</p>
</div>

<div class="yip-pages yip-home">

  <div class="section cover"
    style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('/images/yip/cover.jpg')">
    <div class="item left">
      <div class="row">
        <p class="title">About</p>
        <div class="divider"></div>
        <p class="subtitle">SmartStart<br>Young Innovator<br>Program</p>
      </div>
      <p class="description">The SmartStart Young Innovator Program enables young Cambodian university students to
        launch their own tech startups with the support of Smart Axiata and Impact Hub.
        <br><br>
        The program will improve your capabilities, sharpen your business skills and kick-start your journey to
        developing your very own digital idea. You will also gain access to essential industry knowledge and seed
        funding to begin your own venture.
      </p>
      <div class="image" style="background-image: url('/images/yip/yip.svg')">

      </div>
    </div>
    <div class="item right">
      <div class="btn" style="background-image: url('/images/client/sim-cut-green.png')"
        onclick="window.open('https://form.typeform.com/to/fC6DaSU1')">
        <p class="text">Apply Now</p>
        <div class="icon" style="background-image: url('/images/client/right-white.png')"></div>
      </div>
    </div>
  </div>

  <div id="objectives" class="section border border-bottom why-smartstart">
    <div class="title-line"></div>
    <p class="section-title">
      Why SmartStart Young Innovator Program?
    </p>


    <div id="why-ctn" class="ctn">
      <div class="item">
        <div class="image" style="background-image: url('/images/yip/connect.svg')"></div>
        <p class="title">Connect and Build Relationships</p>
      </div>
      <div class="item">
        <div class="image" style="background-image: url('/images/yip/skills.svg')"></div>
        <p class="title">Gain Technical and Soft Skills</p>
      </div>
      <div class="item">
        <div class="image" style="background-image: url('/images/yip/headstart.svg')"></div>
        <p class="title">Get a Headstart in your Entrepreneurial Journey</p>
      </div>
    </div>

    <div class="slider-btns">
      <div class="btn left" onclick="whySlider(-1)">
        <div class="icon"></div>
        <p class="label">Left</p>
      </div>
      <div class="btn right" onclick="whySlider(1)">
        <p class="label">Right</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>

  <div id="structures" class="section border stake">
    <div class="title-line"></div>
    <p class="section-title">What is at Stake?</p>
    <p class="side-section-title left">
      What is at Stake?
    </p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/hse/objective.png')">

      </div>
      <div class="info">
        <div class="title">Our Objective</div>
        <p class="text">
          The SmartStart Young Innovator Program is aimed at enabling and empowering young Cambodian talents to develop
          their innovative tech and digital ideas with Smart. The goal is to help turn the best concepts into actual
          tech enterprises.
        </p>
        <div class="title">Program Structures</div>
        <p class="text">
        <ol>
          <li>Outreach - Participants will be exposed to digital entrepreneurial concepts and knowledge sharing from
            SmartStart alumni and experienced entrepreneurs.</li>
          <li>Hatch - 120 shortlisted students will join the Hatch to pitch their ideas and form their teams if they
            do not have one.</li>
          <li>Digithon - The selected teams from Hatch will learn about Internet of Things (IoT) and Software and get
            hands-on experience in creating their first prototype.</li>
          <li>Technopreneurship Challenge - 60 selected students will work on realizing their startup ideas with
            business and technical mentorship support throughout the five-day Bootcamp.</li>
          <li>Final Pitch - Five teams will be granted 3-month paid internships with leading digital and tech startups
            to develop their business prototypes.</li>
          <li>The Grand Final Pitch - Top 5 teams will give their business pitch to stand a chance to win 10,000 USD
            and a visit to a tech startup event in the region.</li>
        </ol>
        </p>
      </div>
    </div>
  </div>

  <div class="section border process">
    <div class="title-line"></div>
    <p class="section-title">The Process</p>
    <p class="side-section-title right">
      The Process
    </p>
    <div id="process-ctn" class="ctn">
      <div class="item">
        <div class="image" style="background-image: url('/images/yip/inspire.svg')"></div>
        <p class="title">Inspire</p>
        <p class="description">A total of 120 individuals
          will be selected from a pool
          of online . . .</p>
        <div class="btn" onclick="openProcessCard('inspire')">
          <p class="text">Learn more</p>
          <div class="icon more"></div>
        </div>
      </div>

      <div class="item white">
        <div class="image" style="background-image: url('/images/yip/enable.svg')"></div>
        <p class="title">Enable</p>
        <p class="description">The five-day off-site Technopreneurship Challenge will be shaped around the...</p>
        <div class="btn" onclick="openProcessCard('enable')">
          <p class="text">Learn more</p>
          <div class="icon more gray"></div>
        </div>
      </div>

      <div class="item">
        <div class="image" style="background-image: url('/images/yip/grow.svg')"></div>
        <p class="title">Grow</p>
        <p class="description">Participants will be exposed to real working experiences and environments...</p>
        <div class="btn" onclick="openProcessCard('grow')">
          <p class="text">Learn more</p>
          <div class="icon more"></div>
        </div>
      </div>
    </div>



    <div class="slider-btns">
      <div class="btn left" onclick="processSlider(-1)">
        <div class="icon"></div>
        <p class="label">Left</p>
      </div>
      <div class="btn right" onclick="processSlider(1)">
        <p class="label">Right</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>

  <div id="requirements" class="section border requirements">
    <div class="title-line"></div>
    <p class="section-title">What are we looking for?</p>
    <p class="side-section-title left">
      What are we looking for?
    </p>
    <p class="small-section-title">All university students with a keen interest and curiosity in innovation,
      entrepreneurship, startups and digital technologies are encouraged to apply.</p>
    <div class="ctn mobile" style="background-image: url('/images/hse/benefits-mobile.png')">
      <div class="item tl">Digital education</div>
      <div class="item tr">Digital content and entertainment</div>
      <div class="item bl">Other disruptive industry models</div>
      <div class="item br">Digital commerce and payments</div>
    </div>

    <div class="ctn web" style="background-image: url('/images/hse/benefits.png')">
      <div class="item tl">Digital education</div>
      <div class="item tr">Digital content and entertainment</div>
      <div class="item bl">Other disruptive industry models</div>
      <div class="item br">Digital commerce and payments</div>
    </div>
  </div>

  <div id="criteria" class="section border apply">
    <div class="content-ctn">
      <p class="section-title">Who can apply?</p>
      <p class="small-section-title">At the minimum, candidates must have:</p>
      <div class="ctn">
        <div class="item">
          <div class="image" style="background-image: url('/images/yip/wca1.svg')"></div>
          <p class="title">A high level of English proficiency</p>
        </div>

        <div class="item">
          <div class="image" style="background-image: url('/images/yip/wca2.svg')"></div>
          <p class="title">The drive and determination to succeed</p>
        </div>

        <div class="item">
          <div class="image" style="background-image: url('/images/yip/wca3.svg')"></div>
          <p class="title">Entrepreneurial skills</p>
        </div>
      </div>
    </div>
  </div>

  <div id="previous" class="section border awardee">
    <div class="title-line"></div>
    <p class="section-title">Previous Awardees</p>
    <div id="awardee-ctn" class="content-ctn">
      <div class="item">
        <p class="yellow-title">Cycle 1</p>
        <div class="ctn">
          <div class="awardee" style="background-image: url('/images/yip/awardees/GoSoccer.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Ligo.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Prestige.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Propey.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Spare.png')"></div>
        </div>
      </div>
      <div class="item">
        <p class="yellow-title">Cycle 2</p>
        <div class="ctn">
          <div class="awardee" style="background-image: url('/images/yip/awardees/Haystome.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/HomeX.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Malis.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/SomJot.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Tosrean.png')"></div>
        </div>
      </div>
      <div class="item">
        <p class="yellow-title">Cycle 3</p>
        <div class="ctn">
          <div class="awardee" style="background-image: url('/images/yip/awardees/RenTech.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/PhumCake.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/SersJbong.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Speakout.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Style.png')"></div>
        </div>
      </div>

      <div class="item">
        <p class="yellow-title">Cycle 4</p>
        <div class="ctn">
          <div class="awardee" style="background-image: url('/images/yip/awardees/Adero.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/FlexFloc.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/iCompost.jpg')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/KELA.jpg')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/YourSpace.png')"></div>
        </div>
      </div>
    </div>

    <div class="slider-btns yellow">
      <div class="btn left" onclick="awardeeSlider(-1)">
        <div class="icon"></div>
        <p class="label">Left</p>
      </div>
      <div class="btn right" onclick="awardeeSlider(1)">
        <p class="label">Right</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>

  <div id="faq" class="section border faq">
    <div class="content-ctn">
      <div class="section-title">Frequently Asked Questions</div>
      <div id="group-program" class="group">
        <div class="title-row">
          <div class="image" style="background-image: url('/images/yip/program.svg')"></div>
          <p class="title">The Program</p>
        </div>
        <div class="q">1. What is the objective of this program?</div>
        <div class="a">The SmartStart Young Innovator Program is aimed at enabling and empowering young Cambodian
          talents
          to develop their innovative tech and digital ideas with Smart. The goal is to help turn the best concepts into
          actual tech enterprises.</div>
        <div class="q">2. What are the benefits of joining this program?</div>
        <div class="a">
          <p> SmartStart offers a unique learning platform along with mentorship and financial support. Through
            this program, you will get:</p>
          <p><span>Connected: </span>You will be exposed to various elements of Cambodia’s digital startup ecosystem,
            such as experienced mentors, inspirational speakers, co-working spaces, investors and technical support.</p>
          <p><span>Skills: </span>The program consists of business modules taught by experts in their field who have
            trained and mentored successful startups in Cambodia. The Technopreneurship Challenge will take successful
            teams to a province outside Phnom Penh. (This is subject to change upon consideration of the organizing
            committee in light of the Covid-19 pandemic)</p>
          <p><span>Funding: </span>Five winning teams will be granted a 3-month paid internship with the leading
            digital and tech startups in Cambodia. The winning team of the Grand Final Pitch will receive 10,000 USD as
            well as an array of resources, mentorships and collaborative opportunities. </p>
        </div>
        <div class="q">3. Do I have to bear any costs to participate in this program?</div>
        <div class="a">Smart will cover all program-related expenses, including meals, refreshments and program
          materials. Transportation and accommodation will also be provided for the Technopreneurship Challenge during
          the Enable Phase.</div>
        <div class="q">4. What is the selection process like?</div>
        <div class="a">
          <p class="subtitle">Inspire Phase:</p>
          <p>A total of 120 students will be selected from a pool of applicants to participate in a one-day event called
            Hatch, during which they will pitch their ideas and form their respective teams based on interests and
            skills.
          </p>
          <p>
            Teams will be invited to a two-day Digithon, which will involve prototyping and performing a demonstration.
            After this event, up to 15 ideas/teams will proceed to the next phase for further development.
          </p>

          <p class="subtitle">Enable Phase:</p>
          <p>This phase will see a five-day off-site Technopreneurship Challenge take place. It will be shaped around
            the fundamentals of ideating and growing a digital startup; recognizing, evaluating and exploiting
            opportunities; understanding the importance of rapid prototyping; market validation; and experiencing all
            aspects of business creation. During this phase, there will be high-level digital training and exposure to
            the basics of back-end and front-end software tools.
          </p>
          <p>
            Participants will hear from guest speakers – local and international – and network with mentors and fellow
            entrepreneurs, with a focus on “learning by doing”. The phase will conclude with a public pitching event in
            Phnom Penh after the five-day Bootcamp. At the event, five teams with the most outstanding and promising
            digital ideas will be granted three-month internships with the leading digital and tech startups in Cambodia
            to further develop their business ideas.
          </p>

          <p class="subtitle">Growth Phase:</p>
          <p>In this phase, teams will develop and realize their ideas in a three-month fully paid internship as well as
            receive tailored support in partnership with Impact Hub Phnom Penh. They will turn their startup ideas into
            actual businesses/products through an ecosystem of resources, mentorships, inspiration and collaboration
            with business and tech consultants.
          </p>
          <p>
            Besides having regular check-ins to monitor progress and troubleshoot challenges, the teams will take part
            in demo days and meet with experts and mentors from various companies to receive valuable insights.
          </p>
          <p>
            Teams will enjoy full membership at Impact Hub Phnom Penh, allowing them full-time use of facilities as well
            as free access to regular events, workshops and business clinics.
          </p>
        </div>
        <div class="q">5. What are the requirements for participation if I am selected?</div>
        <div class="a">If you are selected and continually progress, you will be required to fully participate in all
          events of this program, which is divided into three phases and includes the Hatch, Digithon ,
          Technopreneurship Bootcamp and respective pitching events, as outlined above.</div>
        <div class="group-end" onclick="expandFAQ('program')">
          <div class="image"></div>
        </div>
      </div>
      <div id="group-application" class="group">
        <div class="title-row">
          <div class="image" style="background-image: url('/images/yip/application.svg')"></div>
          <p class="title">The Application</p>
        </div>
        <div class="q">1. When is the application deadline?</div>
        <div class="a">27<sup>th</sup> May, 2022.</div>
        <div class="q">2. Who can apply?</div>
        <div class="a">
          <p>This program is open to all university students (with those in the third or fourth year of their studies
            especially encouraged to apply) with:</p>
          <ul>
            <li>A keen interest in innovation, entrepreneurship, startups and digital technologies.</li>
            <li>Any digital business idea in the following verticals: digital education, digital commerce and payments,
              digital entertainment and content, and other disruptive industry models (in the areas of healthcare,
              agriculture or transportation).</li>
            <li>Digital or entrepreneurial skills that may contribute towards the development of one’s ideas. For
              example, coding, graphic/web design, marketing, writing or sales.</li>
            <li>Good English proficiency.</li>
            <li>A strong drive and determination to make their dreams come true.</li>
            <li>A commitment to fully participate in all sessions of the program.</li>
          </ul>
        </div>
        <div class="q">3. How do I apply to this program?</div>
        <div class="a">
          We only accept online applications. <a href="https://form.typeform.com/to/fC6DaSU1" target="_blank">Click
            here</a> if you’d like to get started on your
          application. The deadline
          for applications is 27<sup>th</sup> May, 2022.
        </div>
        <div class="q">4. What supporting materials are required for the application?</div>
        <div class="a">You are required to submit a video introduction about yourself (please refer to the application
          form for details regarding video content). You must upload your video to YouTube or provide a Google Drive
          link accordingly in the application form. Make sure to double check the sharing options so the link is
          accessible.
        </div>
        <div class="q">5. When and how will I be notified of my admission status?</div>
        <div class="a">You will be informed of your application’s status by the organizers via email. Successful
          applicants will be asked to confirm their participation upon receiving the notification. Failure to do so will
          imply that the applicant has rejected the opportunity to join the program, and the position will be offered to
          other applicants.</div>
        <div class="q">6. Can I apply as an individual or do I need to have a team?</div>
        <div class="a">You can apply to SmartStart as an individual, or as part of a team.If you apply as an individual,
          you can apply with or without an idea. If you apply as a team, you must apply with an idea. If you apply as a
          team, we will either accept your idea and your whole team, or we may reject your idea but consider each
          applicant individually.</div>
        <div class="q">7. Can I apply without an idea?</div>
        <div class="a">You can apply to SmartStart without an idea only if you are applying as an individual.</div>
        <div class="q">8. How do I find a good idea?</div>
        <div class="a">A great idea is something that solves a problem that someone is facing. You can think about a
          problem that you, your family members or friends used to face. A good business idea could also be an
          enterprise that exists online or in another country that you think should exist here. Another suggestion is to
          think about what you’re passionate about and how you can turn that passion into a business. </div>
        <div class="q">9. I’m stuck with my application -- help!</div>
        <div class="a">
          <p>Here are some of our suggestions:</p>
          <ul>
            <li>Make sure your problem is real, specific enough and has a real market with potential customers. Make
              sure you have spoken to people who experience the problem or experts who are working in that area for
              validation! </li>
            <li>You should think outside of the box and come up with solutions that are innovative and creative. We
              recommend you to do some research on what has been done, and how you can make those solutions better.
            </li>
            <li>Make sure to have some data, facts and figures on how big the problem is. </li>
            <li>Lastly, make sure the problem is something you and your team are passionate about -- but also make sure
              your team has the right skills and experience to pull it off. </li>
          </ul>
        </div>
        <div class="q">10. How do I know my team is the right fit for the program?</div>
        <div class="a">
          <p>See this checklist. Your team has: </p>
          <ul>
            <li>People with diverse and complementary skills and backgrounds (at least one person with creative, one
              with business and one with tech skills).</li>
            <li>At least more than half of the team is very passionate about the topic.</li>
            <li>You have the time to commit to this project and you are committed.</li>
            <li>You have the same vision. </li>
            <li>At least one of the team members has some industry insights.</li>
          </ul>
        </div>
        <div class="q">What are the important dates?</div>
        <div class="a">
          <p><span>Hatch: </span> June 11<sup>th</sup> - 12<sup>th</sup>, 2022</p>
          <p><span>Digithon: </span> June 17<sup>th</sup> - 19<sup>th</sup>, 2022</p>
          <p><span>Technopreneurship Challenge: </span> June 28<sup>th</sup> - July 3<sup>rd</sup>, 2022</p>
          <p><span>Final Pitch: </span> July 10<sup>th</sup>, 2022</p>
          <p><span>Internship: </span> August - October, 2022</p>
          <p><span>Grand Final Pitch: </span> November 12<sup>th</sup>, 2022</p>
          <p>The organizers reserve the rights to change the dates. In conducting this program, the organizers will
            closely monitor the situation of the spread of coronavirus and will shift arrangements following the
            standard operating procedures issued by the Ministry of Health.
          </p>
        </div>
        <div class="group-end" onclick="expandFAQ('application')">
          <div class="image"></div>
        </div>
      </div>
    </div>
  </div>

  <div id="apply" class="section apply-now"
    style="background: linear-gradient( rgba(0, 154, 62, 1),rgba(0, 154, 62, 0), rgba(0, 154, 62, 0) ), url('/images/client/p-7.jpg')">
    <p class="title">Are You Ready To Innovate With Smart?</p>

    <div class="btn" style="background-image: url('/images/client/sim-cut-green.png')"
      onclick="window.open('https://form.typeform.com/to/fC6DaSU1')">
      <p class="text">Apply Now</p>
      <div class="icon" style="background-image: url('/images/client/right-white.png')"></div>
    </div>
  </div>

  <div class="section partnership">
    <div class="title-line"></div>
    <p class="section-title">In Partnership With</p>
    <div class="ctn">
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/client/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/client/CBRD.png')"></div>
      </div>
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/Impact.png')"></div>
      </div>
      <div class="item">
        <div class="logo smart" style="background-image: url('/images/client/Smart.png')"></div>
      </div>
    </div>

    <div class="tagline-ctn">
      <div class="tagline">Innovating With You, Investing In You!</div>
      {{-- <div class="logo" style="background-image: url('/images/yip/yip.png')"></div> --}}
    </div>
  </div>


  <div id="enable-card" class="card enable">
    <div class="backdrop" onclick="closeProcessCard()"></div>
    <div class="content-ctn">
      <div class="btn close" onclick="closeProcessCard()"></div>
      <div class="image" style="background-image: url('/images/yip/enable.svg')"></div>
      <div class="content">
        <p class="title">Enable</p>

        <p class="text">The five-day off-site Technopreneurship Challenge will be shaped around the fundamentals of
          ideating and growing a digital startup. It will involve:</p>

        <ol>
          <li>Identifying, evaluating and capitalizing on opportunities.</li>
          <li>Understanding the importance of rapid prototyping.
          </li>
          <li>Market validation and business creation.</li>
          <li>Guest speakers and networking with mentors and fellow entrepreneurs, with a focus on “learning by doing”.
            ​​​​​​​This includes high-level digital training and exposure to the basics of back-end and front-end
            software tools.</li>
        </ol>

        <p class="text">
          A pitching event in Phnom Penh will shortlist five of the most outstanding teams, and they will proceed to the
          3-month internship program, which they will be placed with leading digital and tech startups to develop their
          business prototype.
        </p>
      </div>
    </div>
  </div>

  <div id="inspire-card" class="card inspire">
    <div class="backdrop" onclick="closeProcessCard()"></div>
    <div class="content-ctn">
      <div class="btn close" onclick="closeProcessCard()"></div>
      <div class="image" style="background-image: url('/images/yip/inspire.svg')"></div>
      <div class="content">
        <p class="title">Inspire</p>
        <p class="text">A total of 120 individuals will be selected from a pool of online applicants to participate in a
          one-day event called Hatch. Selected participants with business ideas will pitch theirs in front a panel of
          judges. Once the ideas are selected by judges, they can recruit their new members for the teams that are
          vacant based on their interest and skills matching with their startup ideas. The teams with the most promising
          startup ideas will be invited to the two-day Digithon, where 15 teams will be selected by a panel of judges in
          the digital ecosystem to be part of the five-day stay-in Technopreneurship Challenge.</p>
      </div>
    </div>
  </div>

  <div id="grow-card" class="card grow">
    <div class="backdrop" onclick="closeProcessCard()"></div>
    <div class="content-ctn">
      <div class="btn close" onclick="closeProcessCard()"></div>
      <div class="image" style="background-image: url('/images/yip/grow.svg')"></div>
      <div class="content">
        <p class="title">Grow</p>
        <p class="text">Participants will be exposed to real working experiences and environments at cutting-edge
          startups and businesses in Cambodia's thriving startup ecosystem. At the end of the internship, one start-up
          will stand a chance to win 10,000 USD to continue working on their business ideas.</p>
      </div>
    </div>
  </div>
</div>
@endsection
