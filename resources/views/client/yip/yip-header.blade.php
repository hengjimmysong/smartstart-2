<div class="header yip-header" data-scroll-pos="0">
  <div class="logo" style="background-image: url('/images/yip/yip.png')" onclick="customScrollTo('top')"></div>
  <div id="menu" class="menu">
    <div id="h-overview" class="btn item" onclick="customScrollTo('top')" data-scrolled-passed="true">Overview</div>
    <div id="h-objectives" class="btn item" onclick="customScrollTo('objectives')">Objectives</div>
    <div id="h-structures" class="btn item" onclick="customScrollTo('structures')">Structures</div>
    <div id="h-requirements" class="btn item" onclick="customScrollTo('requirements')">Requirements</div>
    <div id="h-criteria" class="btn item" onclick="customScrollTo('criteria')">Criteria</div>
    <div id="h-previous" class="btn item" onclick="customScrollTo('previous')">Previous Winners</div>
    <div id="h-faq" class="btn item" onclick="customScrollTo('faq')">FAQs</div>
    <div id="h-apply" class="btn item" onclick="customScrollTo('apply')">Apply Now</div>
    <div id="h-lang" class="btn item lang" onclick="window.location.href = '/kh/yip'"
      style="background-image: url('/images/client/eng.png')"></div>
  </div>
  <div class="menu-btn" onclick="toggleNavBar()"></div>
</div>
