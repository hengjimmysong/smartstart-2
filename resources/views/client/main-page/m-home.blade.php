@extends('layouts.main-page.main-page-layout')
@section('header')
<title>SmartStart</title>
@endsection

@section('content')
<div class="m-pages m-home">
  <div id="home" class="section cover"
    style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('/images/client/p-1.jpg')">
    {{-- <div class="title">
      <p class="green">Smart</p>
      <p class="white">Start</p>
    </div> --}}
    <div class="title-img" style="background-image: url('/images/main-page/logo-white.png')"></div>
    <div class="divider"></div>
    <div class="description">Elevating Cambodia’s digital economy in the Industry 4.0 era through tech innovation</div>
  </div>
  <div id="about" class="section about-us">
    <div class="title-line"></div>
    <p class="section-title">About Us</p>
    <div id="about-us-ctn" class="ctn">
      <div class="item">
        <div class="title">Mission</div>
        <div class="icon" style="background-image: url('/images/main-page/mission.png"></div>
        <div class="divider"></div>
        <div class="desc-ctn">
          <p class="description">SmartStart equips young Cambodians with digital and entrepreneurial skills through
            immersive practical experiences in Hackathons, Bootcamps and online blended learning courses so they can
            become the future digital talents and leaders of Cambodia.</p>
        </div>
      </div>

      <div class="item">
        <div class="title">Vision</div>
        <div class="icon" style="background-image: url('/images/main-page/vision.png"></div>
        <div class="divider"></div>
        <div class="desc-ctn">
          <p class="description">In collaboration with the Ministry of Posts and Telecommunications and the Ministry of
            Education, Youth and Sport, Smart Axiata envisions developing competent talents who will contribute to
            elevating Cambodia’s digital growth in the Fourth Industrial Revolution era through SmartStart programs.</p>
        </div>
      </div>

      <div class="item">
        <div class="title">History</div>
        <div class="icon" style="background-image: url('/images/main-page/history.png"></div>
        <div class="divider"></div>
        <div class="desc-ctn">
          <p class="description">Established in 2017, SmartStart enables Cambodian university students to launch digital
            startups by offering unique learning experiences through mentorships and financial support. SmartStart has
            expanded to provide entrepreneurial education to more young Cambodians and has impacted about a thousand
            youths.</p>
        </div>
      </div>
    </div>
    <div class="slider-btns">
      <div class="btn left" onclick="aboutUsSlider(-1)">
        <div class="icon"></div>
        <p class="label">Left</p>
      </div>
      <div class="btn right" onclick="aboutUsSlider(1)">
        <p class="label">Right</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>

  <div id="programs" class="section program">
    <div class="title-line"></div>
    <p class="section-title">Programs</p>
    <div class="ctn"
      style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('/images/client/p-3.jpg')">
      <div class="item" onclick="window.location.href = '/yip'">
        <p class="title">SmartStart <br>Young Innovator Program</p>
      </div>
      <div class="item" onclick="window.location.href = '/kh/hse'">
        <p class="title">SmartStart <br>High School Edition</p>
      </div>
      <div class="item" onclick="window.location.href = '/ulp'">
        <p class="title">SmartStart <br>Unipreneur Learning Platform</p>
      </div>
    </div>
  </div>

  <div id="contact" class="section contact-us">
    <div class="title-line"></div>
    <p class="section-title">Contact Us</p>
    <div class="ctn">
      <div class="item">
        <div class="icon" style="background-image: url('/images/main-page/pin.png')"></div>
        <div class="info">
          <p class="title">Visit Us</p>
          <p class="value">No. 464A Monivong Blvd,<br>
            Sangkat Tonle Bassac,<br>
            Khan Chamkarmorn,<br>Phnom Penh
            Cambodia</p>
        </div>
      </div>

      <div class="divider"></div>

      <div class="item">
        <div class="icon" style="background-image: url('/images/main-page/phone.png')"></div>
        <div class="info">
          <p class="title">Call Us</p>
          <p class="value short">+855 (0) 10 234 075</p>
        </div>
      </div>

      <div class="divider"></div>

      <div class="item">
        <div class="icon" style="background-image: url('/images/main-page/mail.png')"></div>
        <div class="info">
          <p class="title">Email Us</p>
          <p class="value short">sustainability@smart.com.kh</p>
        </div>
      </div>
    </div>
    <div class="map-ctn">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.197852927056!2d104.92135141494555!3d11.537660347835356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310950dbd81ec78d%3A0x8d3ccf621a2b4e1f!2sSmart+Axiata!5e0!3m2!1sen!2skh!4v1552488128259"
        frameborder="0" style="border:0" allowfullscreen class="frame"></iframe>
    </div>
  </div>
  <div class="section fill-your-info">
    <div class="title-line"></div>
    <p class="section-title">Contact Us</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/main-page/form.png')"></div>
      <form class="form" action="/contacts" method="POST">
        {{csrf_field()}}
        <input type="text" name="name" class="input" placeholder="Your Name" required>
        <input type="text" name="phone" class="input" placeholder="Phone Number" required>
        <input type="text" name="email" class="input" placeholder="Email Address" required>
        <textarea name="message" class="input text" placeholder="Message" rows="3" required></textarea>
        <input type="text" name="source" style="display: none" value="main">

        <input type="submit" value="Compose" class="btn submit">
      </form>
    </div>
  </div>
  <div class="section partnership">
    <div class="title-line"></div>
    <p class="section-title">In Partnership With</p>
    <div class="ctn">
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/client/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/client/CBRD.png')"></div>
      </div>
      <div class="item">
        <div class="logo smart" style="background-image: url('/images/client/Smart.png')"></div>
      </div>
    </div>
  </div>

</div>
@endsection

