<script>
  aboutUsContainer = document.getElementById("about-us-ctn")
  
  var aboutUsSliderValues = ["33%", "0%", "-33%"]
  var currentIndex = 0
  
  function aboutUsSlider(param) {
    if(currentIndex + param < 0 ||currentIndex + param > 2 ){
      return
    }


    currentIndex += param
    var value = aboutUsSliderValues [currentIndex]
    aboutUsContainer.style.transform = `translateX(${value})`
  }

  var scrollOffset = 60;
  function customScrollTo(param){
    if(param == 'top'){
      window.scrollTo({
        top: 0,
        behavior: "smooth"
      })
    } else {
      var element = document.getElementById(param);
      var elementPosition = element.getBoundingClientRect().top;
      var offsetPosition = window.pageYOffset + elementPosition - scrollOffset;
      
      window.scrollTo({
          top: offsetPosition,
          behavior: "smooth"
      });
    }
    checkScroll()
    closeNavBar()
  }
  aboutUs = document.getElementById('about')
  programs = document.getElementById('programs')
  contactUs = document.getElementById('contact')
  homeH = document.getElementById('h-home')
  aboutUsH = document.getElementById('h-about')
  programsH = document.getElementById('h-programs')
  contactUsH = document.getElementById('h-contact')

  var currentActive = "h-home"


  window.onscroll = function() {
    checkScroll()
  }

  function checkScroll() {
    var aboutUsPos = aboutUs.getBoundingClientRect().top - scrollOffset - 2;
    var programsPos = programs.getBoundingClientRect().top - scrollOffset - 2;
    var contactUsPos = contactUs.getBoundingClientRect().top - scrollOffset - 2;
    homeH.setAttribute('data-scrolled-passed', aboutUsPos > 0 && programsPos > 0 && contactUsPos > 0)
    aboutUsH.setAttribute('data-scrolled-passed', aboutUsPos < 0 && programsPos > 0 && contactUsPos > 0)
    programsH.setAttribute('data-scrolled-passed', programsPos < 0 && contactUsPos > 0)
    contactUsH.setAttribute('data-scrolled-passed', contactUsPos < 0)
  }

  var isNavBarOpened = false
  navBar = document.getElementById('menu')


  function toggleNavBar () {
    isNavBarOpened = !isNavBarOpened

    if(isNavBarOpened){
      navBar.classList.add('active')
    } else {
      navBar.classList.remove('active')
    }
  }

  function closeNavBar () {
    isNavBarOpened = false
    navBar.classList.remove('active')
  }
</script>
