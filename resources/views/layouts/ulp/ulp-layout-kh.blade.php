<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  @yield('header')
  <link rel="icon" href="{{asset('/images/client/prism.png')}}">

  <!-- Sripts -->
  <script src="{{ asset('js/main.js') }}" defer></script>
  <!-- Styles -->
  <link href="{{ asset('css/ulp-pages.css') }}" rel="stylesheet">
  <link href="{{ asset('css/client.css') }}" rel="stylesheet">
</head>

<body>
  @if(session()->has('message'))
    <div class="alert">{{ session()->get('message') }}</div>
  @endif

  @include('client-kh.ulp.ulp-header')
  @yield('content')
  @include('client-kh.components.footer', ['page' => 'High School Edition'])
  @include('layouts.ulp.script')
</body>

</html>
