<script>

  var scrollOffset = 60;
  function customScrollTo(param){
    if(param == 'top'){
      window.scrollTo({
        top: 0,
        behavior: "smooth"
      })
    } else {
      var element = document.getElementById(param);
      var elementPosition = element.getBoundingClientRect().top;
      var offsetPosition = window.pageYOffset + elementPosition - scrollOffset;
      
      window.scrollTo({
          top: offsetPosition,
          behavior: "smooth"
      });
    }
    checkScroll()
    closeNavBar()
  }
  about = document.getElementById('about')
  objective = document.getElementById('objective')
  testimonials = document.getElementById('testimonials')
  faq = document.getElementById('faq')
  homeH = document.getElementById('h-home')
  aboutH = document.getElementById('h-about')
  objectiveH = document.getElementById('h-objective')
  testimonialsH = document.getElementById('h-testimonials')
  faqH = document.getElementById('h-faq')

  var currentActive = "h-home"


  window.onscroll = function() {
    checkScroll()
  }

  function checkScroll() {
    var aboutPos = about.getBoundingClientRect().top - scrollOffset - 2;
    var objectivePos = objective.getBoundingClientRect().top - scrollOffset - 2;
    var testimonialsPos = testimonials.getBoundingClientRect().top - scrollOffset - 2;
    homeH.setAttribute('data-scrolled-passed', aboutPos > 0 && objectivePos > 0 && testimonialsPos > 0)
    aboutH.setAttribute('data-scrolled-passed', aboutPos < 0 && objectivePos > 0 && testimonialsPos > 0)
    objectiveH.setAttribute('data-scrolled-passed', objectivePos < 0 && testimonialsPos > 0)
    testimonialsH.setAttribute('data-scrolled-passed', testimonialsPos < 0)
  }

  var isNavBarOpened = false
  navBar = document.getElementById('menu')


  function toggleNavBar () {
    isNavBarOpened = !isNavBarOpened

    if(isNavBarOpened){
      navBar.classList.add('active')
    } else {
      navBar.classList.remove('active')
    }
  }

  function closeNavBar () {
    isNavBarOpened = false
    navBar.classList.remove('active')
  }
</script>
