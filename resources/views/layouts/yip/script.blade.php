<script>
  whyContainer = document.getElementById("why-ctn")
  var whySliderValues = ["33.5%", "0%", "-33.5%"]
  var currentIndex = 0
  
  function whySlider(param) {
    if(currentIndex + param < 0 ||currentIndex + param > 2 ){
      return
    }


    currentIndex += param
    var value = whySliderValues [currentIndex]
    whyContainer.style.transform = `translateX(${value})`
  }

  processContainer = document.getElementById("process-ctn")
  var processSliderValues = ["36%", "0%", "-36%"]
  var currentIndex = 0
  
  function processSlider(param) {
    if(currentIndex + param < 0 ||currentIndex + param > 2 ){
      return
    }


    currentIndex += param
    var value = processSliderValues [currentIndex]
    processContainer.style.transform = `translateX(${value})`
  }

  awardeeContainer = document.getElementById("awardee-ctn")
  var awardeeSliderValues = ["0%", "-100%", "-200%", "-300%"]
  var currentIndex = 3
  awardeeContainer.style.transform = `translateX(${awardeeSliderValues[currentIndex]})`
  
  function awardeeSlider(param) {
    currentIndex += param

    if(currentIndex < 0){
      currentIndex = 3
    }else if (currentIndex > 3 ){
      currentIndex = 0
    }

    var value = awardeeSliderValues[currentIndex]
    awardeeContainer.style.transform = `translateX(${value})`
  }

  enableCard = document.getElementById("enable-card")
  inspireCard = document.getElementById("inspire-card")
  growCard = document.getElementById("grow-card")

  function openProcessCard(param) {
    if(param == 'enable'){
      enableCard.classList.add('active');
      inspireCard.classList.remove('active')
      growCard.classList.remove('active')
    } 

    if(param == 'inspire'){
      inspireCard.classList.add('active')
      enableCard.classList.remove('active')
      growCard.classList.remove('active')
    } 

    if(param == 'grow'){
      growCard.classList.add('active')
      enableCard.classList.remove('active')
      inspireCard.classList.remove('active')
    } 
  }

  function closeProcessCard(){
    enableCard.classList.remove('active')
    inspireCard.classList.remove('active')
    growCard.classList.remove('active')
  }

  groupProgram = document.getElementById('group-program')
  groupApplication = document.getElementById('group-application')

  function expandFAQ(param) {
    if(param == 'program') {
      if(groupProgram.classList.contains('expanded')){
        groupProgram.classList.remove('expanded')
      } else{
        groupProgram.classList.add('expanded')
      }
    }

    if(param == 'application') {
      if(groupApplication.classList.contains('expanded')){
        groupApplication.classList.remove('expanded')
      } else{
        groupApplication.classList.add('expanded')
      }
    }
  }


  var scrollOffset = 60;
  function customScrollTo(param){
    if(param == 'top'){
      window.scrollTo({
        top: 0,
        behavior: "smooth"
      })
    } else {
      var element = document.getElementById(param);
      var elementPosition = element.getBoundingClientRect().top;
      var offsetPosition = window.pageYOffset + elementPosition - scrollOffset;
      
      window.scrollTo({
          top: offsetPosition,
          behavior: "smooth"
      });
    }
    checkScroll()
    closeNavBar()
  }
  objectives = document.getElementById('objectives')
  structures = document.getElementById('structures')
  requirements = document.getElementById('requirements')
  criteria = document.getElementById('criteria')
  previous = document.getElementById('previous')
  faq = document.getElementById('faq')
  apply = document.getElementById('apply')
  overviewH = document.getElementById('h-overview')
  objectivesH = document.getElementById('h-objectives')
  structuresH = document.getElementById('h-structures')
  requirementsH = document.getElementById('h-requirements')
  criteriaH = document.getElementById('h-criteria')
  previousH = document.getElementById('h-previous')
  faqH = document.getElementById('h-faq')
  applyH = document.getElementById('h-apply')

  var currentActive = "h-overview"


  window.onscroll = function() {
    checkScroll()
  }

  function checkScroll() {
    var objectivesPos = objectives.getBoundingClientRect().top - scrollOffset - 2;
    var structuresPos = structures.getBoundingClientRect().top - scrollOffset - 2;
    var requirementsPos = requirements.getBoundingClientRect().top - scrollOffset - 2;
    var criteriaPos = criteria.getBoundingClientRect().top - scrollOffset - 2;
    var previousPos = previous.getBoundingClientRect().top - scrollOffset - 2;
    var faqPos = faq.getBoundingClientRect().top - scrollOffset - 2;
    var applyPos = apply.getBoundingClientRect().top - scrollOffset - 2;
    overviewH.setAttribute('data-scrolled-passed', objectivesPos > 0 && structuresPos > 0 && requirementsPos > 0 && criteriaPos > 0 && previousPos > 0 && faqPos > 0 && applyPos > 0)
    objectivesH.setAttribute('data-scrolled-passed', objectivesPos < 0 && structuresPos > 0 && requirementsPos > 0 && criteriaPos > 0 && previousPos > 0 && faqPos > 0 && applyPos > 0)
    structuresH.setAttribute('data-scrolled-passed', structuresPos < 0 && requirementsPos > 0 && criteriaPos > 0 && previousPos > 0 && faqPos > 0 && applyPos > 0)
    requirementsH.setAttribute('data-scrolled-passed', requirementsPos < 0 && criteriaPos > 0 && previousPos > 0 && faqPos > 0 && applyPos > 0)
    criteriaH.setAttribute('data-scrolled-passed', criteriaPos < 0 && previousPos > 0 && faqPos > 0 && applyPos > 0)
    previousH.setAttribute('data-scrolled-passed', previousPos < 0 && faqPos > 0 && applyPos > 0)
    faqH.setAttribute('data-scrolled-passed', faqPos < 0 && applyPos > 0)
    applyH.setAttribute('data-scrolled-passed', applyPos < 0)
  }

  var isNavBarOpened = false
  navBar = document.getElementById('menu')


  function toggleNavBar () {
    isNavBarOpened = !isNavBarOpened

    if(isNavBarOpened){
      navBar.classList.add('active')
    } else {
      navBar.classList.remove('active')
    }
  }

  function closeNavBar () {
    isNavBarOpened = false
    navBar.classList.remove('active')
  }
</script>
