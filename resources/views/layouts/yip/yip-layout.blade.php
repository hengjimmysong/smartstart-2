
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  @yield('header')
  <link rel="icon" href="{{asset('/images/client/prism.png')}}">

  <!-- Sripts -->
  <script src="{{ asset('js/main.js') }}" defer></script>
  <!-- Styles -->
  <link href="{{ asset('css/yip-pages.css') }}" rel="stylesheet">
  <link href="{{ asset('css/client.css') }}" rel="stylesheet">
</head>

<body>
  @if(session()->has('message'))
  <div class="alert">{{ session()->get('message') }}</div>
  @endif

  @include('client.yip.yip-header')
  @yield('content')
  @include('client.components.footer', ['page' => 'Young Innovator Program'])
  @include('layouts.yip.script')
</body>

</html>
