<script>
  structureContainer = document.getElementById("structure-ctn")
  var structureSliderValues = ["33.5%", "0%", "-33.5%"]
  var currentIndex = 0
  
  function structureSlider(param) {
    if(currentIndex + param < 0 ||currentIndex + param > 2 ){
      return
    }


    currentIndex += param
    var value = structureSliderValues [currentIndex]
    structureContainer.style.transform = `translateX(${value})`
  }

  faqContainer = document.getElementById("faq-ctn")
  var faqSliderValues = ["33.5%", "0%", "-33.5%"]
  var currentIndex = 0
  
  function faqSlider(param) {
    if(currentIndex + param < 0 ||currentIndex + param > 2 ){
      return
    }


    currentIndex += param
    var value = faqSliderValues [currentIndex]
    faqContainer.style.transform = `translateX(${value})`
  }

  var scrollOffset = 60;
  function customScrollTo(param){
    if(param == 'top'){
      window.scrollTo({
        top: 0,
        behavior: "smooth"
      })
    } else {
      var element = document.getElementById(param);
      var elementPosition = element.getBoundingClientRect().top;
      var offsetPosition = window.pageYOffset + elementPosition - scrollOffset;
      
      window.scrollTo({
          top: offsetPosition,
          behavior: "smooth"
      });
    }
    checkScroll()
    closeNavBar()
  }
  objectives = document.getElementById('objectives')
  structure = document.getElementById('structure')
  benefits = document.getElementById('benefits')
  criteria = document.getElementById('criteria')
  homeH = document.getElementById('h-home')
  objectivesH = document.getElementById('h-objectives')
  structureH = document.getElementById('h-structure')
  benefitsH = document.getElementById('h-benefits')
  criteriaH = document.getElementById('h-criteria')

  var currentActive = "h-home"


  window.onscroll = function() {
    checkScroll()
  }

  function checkScroll() {
    var objectivesPos = objectives.getBoundingClientRect().top - scrollOffset - 2;
    var structurePos = structure.getBoundingClientRect().top - scrollOffset - 2;
    var benefitsPos = benefits.getBoundingClientRect().top - scrollOffset - 2;
    var criteriaPos = criteria.getBoundingClientRect().top - scrollOffset - 2;
    homeH.setAttribute('data-scrolled-passed', objectivesPos > 0 && structurePos > 0 && benefitsPos > 0 && criteriaPos > 0)
    objectivesH.setAttribute('data-scrolled-passed', objectivesPos < 0 && structurePos > 0 && benefitsPos > 0 && criteriaPos > 0)
    structureH.setAttribute('data-scrolled-passed', structurePos < 0 && benefitsPos > 0 && criteriaPos > 0)
    benefitsH.setAttribute('data-scrolled-passed', benefitsPos < 0 && criteriaPos > 0)
    criteriaH.setAttribute('data-scrolled-passed', criteriaPos < 0)
  }

  var isNavBarOpened = false
  navBar = document.getElementById('menu')


  function toggleNavBar () {
    isNavBarOpened = !isNavBarOpened

    if(isNavBarOpened){
      navBar.classList.add('active')
    } else {
      navBar.classList.remove('active')
    }
  }

  function closeNavBar () {
    isNavBarOpened = false
    navBar.classList.remove('active')
  }
</script>
