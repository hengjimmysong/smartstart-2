@extends('layouts.admin')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          Create Press
        </div>

        <form action="/admin/users" method="POST">
          <div class="col p-3">
            {{csrf_field()}}
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" name="name" class="form-control" id="name" placeholder="Name" required>
            </div>

            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
            </div>

            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" name="password" class="form-control" id="password_confirmation" placeholder="Password" required>
            </div>

            <div class="form-group">
              <label for="password_confirmation">Password Confirmation</label>
              <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirmation" required>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
