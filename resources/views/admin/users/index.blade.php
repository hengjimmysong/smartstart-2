@extends('layouts.admin')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          <div class="row">
            <div class="col">Users</div>
            <a href="/admin/users/create" class="btn btn-primary">Create User</a>
          </div>
        </div>

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($users as $user)
            <tr>
              <th scope="row">{{$user->id}}</th>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>
                <form action="{{url('/admin/users', $user->id)}}" method="POST"
                  onSubmit="return confirm('Are you sure you wish to delete?')">
                  @method('DELETE')
                  {{csrf_field()}}
                  <input type="submit" class="btn btn-danger" value="Delete User" />
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
