@extends('layouts.admin')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          <div class="row">
            <div class="col">Contact</div>
          </div>
        </div>

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Date</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Phone Number</th>
              <th scope="col">Message</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($contacts as $contact)
            <tr>
              <th scope="row">{{$contact->id}}</th>
              <th>{{ date('h:i:s D, d M Y', strtotime($contact->created_at.'-7:00')) }}</th>
              <td>{{$contact->name}}</td>
              <td>{{$contact->phone}}</td>
              <td>{{$contact->email}}</td>
              <td>{{$contact->message}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
