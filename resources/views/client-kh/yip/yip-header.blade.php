
<div class="header yip-header" data-scroll-pos="0">
  <div class="logo" style="background-image: url('/images/yip/yip.png')" onclick="customScrollTo('top')"></div>
  <div id="menu" class="menu">
    <div id="h-overview" class="btn item" onclick="customScrollTo('top')" data-scrolled-passed="true">ទិដ្ឋភាពទូទៅ</div>
    <div id="h-objectives" class="btn item" onclick="customScrollTo('objectives')">គោលបំណង</div>
    <div id="h-structures" class="btn item" onclick="customScrollTo('structures')">រចនាសម្ព័ន្ធ</div>
    <div id="h-requirements" class="btn item" onclick="customScrollTo('requirements')">តម្រូវការ</div>
    <div id="h-criteria" class="btn item" onclick="customScrollTo('criteria')">លក្ខណៈវិនិច្ឆ័យ</div>
    <div id="h-previous" class="btn item" onclick="customScrollTo('previous')">អ្នកឈ្នះជំនាន់មុនៗ</div>
    <div id="h-faq" class="btn item" onclick="customScrollTo('faq')">FAQs</div>
    <div id="h-apply" class="btn item" onclick="customScrollTo('apply')">ដាក់ពាក្យ​ឥឡូវនេះ</div>
    <div id="h-lang" class="btn item lang" onclick="window.location.href = '/yip'"
      style="background-image: url('/images/client/kh.png')"></div>
  </div>
  <div class="menu-btn" onclick="toggleNavBar()"></div>
</div>
