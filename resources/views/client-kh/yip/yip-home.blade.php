@extends('layouts.yip.yip-layout-kh')
@section('header')
<title>SmartStart - YIP</title>
@endsection

@section('content')
<div class="floating-btn kh left" onclick="window.location.href = '/kh'">
  <p class="label">ត្រលប់ទៅទំព័រដើម</p>
  <div class="icon" style="background-image: url('/images/client/left-white.png')"></div>
</div>

<div class="floating-btn kh right" onclick="customScrollTo('top')">
  <div class="icon" style="background-image: url('/images/client/up-white.png')"></div>
  <p class="label">ត្រលប់ទៅខាងលើ</p>
</div>

<div class="yip-pages yip-home yip-home-kh">

  <div class="section cover"
    style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('/images/yip/cover.jpg')">
    <div class="item left">
      <div class="row">
        <p class="title">អំពីកម្មវិធី</p>
        <div class="divider"></div>
        <p class="subtitle">SmartStart<br>Young Innovator<br>Program</p>
      </div>
      <p class="description">គោលបំណងផ្តល់ឱកាសឲ្យនិស្សិតកម្ពុជាដែលមានទេពកោសល្យ និងសមត្ថភាព
        ដើម្បីអភិវឌ្ឍគំនិតច្នៃប្រឌិតអំពីវិស័យ បច្ចេកវិទ្យា និង ឌីជីថល របស់ពួកគេ ជាមួយនឹង ក្រុមហ៊ុន Smart និងImpact Hub
        Phnom Penh ។
        <br><br>
        SmartStart ជាកម្មវិធីតែមួយគត់ ដែលផ្តល់នូវវេទិកាសម្រាប់ សិក្សារៀនសូត្រ និងទទួលបានការជួយ បង្ហាត់បង្ហាញពីអ្នក ជំនាញ
        និងទទួលជំនួយផ្នែកហិរញ្ញវត្ថុទៀតផង។
      </p>
      <div class="image" style="background-image: url('/images/yip/yip.svg')">

      </div>
    </div>
    <div class="item right">
      <div class="btn" style="background-image: url('/images/client/sim-cut-green.png')"
        onclick="window.open('https://form.typeform.com/to/fC6DaSU1')">
        <p class="text">ដាក់ពាក្យ​ឥឡូវនេះ</p>
        <div class="icon" style="background-image: url('/images/client/right-white.png')"></div>
      </div>
    </div>
  </div>

  <div id="objectives" class="section border border-bottom why-smartstart">
    <div class="title-line"></div>
    <p class="section-title">
      ហេតុអ្វីជ្រើសរើសកម្មវិធី SMARTSTART Young Innovator Program?
    </p>


    <div id="why-ctn" class="ctn">
      <div class="item">
        <div class="image" style="background-image: url('/images/yip/connect.svg')"></div>
        <p class="title">ភ្ជាប់និងកសាងទំនាក់ទំនង</p>
      </div>
      <div class="item">
        <div class="image" style="background-image: url('/images/yip/skills.svg')"></div>
        <p class="title">ទទួលបានជំនាញបច្ចេកទេសនិងជំនាញទន់</p>
      </div>
      <div class="item">
        <div class="image" style="background-image: url('/images/yip/headstart.svg')"></div>
        <p class="title">ចាប់ផ្ដើមដំណើរសហគ្រិនភាព</p>
      </div>
    </div>

    <div class="slider-btns">
      <div class="btn left" onclick="whySlider(-1)">
        <div class="icon"></div>
        <p class="label">ឆ្វេង</p>
      </div>
      <div class="btn right" onclick="whySlider(1)">
        <p class="label">ស្តាំ</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>

  <div id="structures" class="section border stake">
    <div class="title-line"></div>
    <p class="section-title">គោលបំណងនិងដំណើរការ</p>
    <p class="side-section-title left">
      គោលបំណងនិងដំណើរការ
    </p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/hse/objective.png')">

      </div>
      <div class="info">
        <div class="title">គោលបំណង</div>
        <p class="text">
          SmartStart គឺជាកម្មវិធីអ្នកច្នៃប្រឌិតវ័យក្មេងមួយដែលមាន គោលបំណងផ្តល់ឱកាសឲ្យនិស្សិតកម្ពុជាដែលមានទេពកោសល្យ
          និងសមត្ថភាព ដើម្បីអភិវឌ្ឍគំនិតច្នៃប្រឌិតអំពីវិស័យ បច្ចេកវិទ្យា និង ឌីជីថល ឲ្យទៅជាការពិត។
        </p>
        <div class="title">រចនាសម្ព័ន្ធកម្មវិធី</div>
        <p class="text">
        <ol>
          <li>Outreach - និស្សិតនឹងស្វែងយល់ពីគំនិតសហគ្រិនភាពបែបឌីជីថល និងទទួលបានការចែករំលែកបទពិសោធន៍ពីអតីតសមាជិករបស់
            SmartStart និងអ្នកសហគ្រិនផ្សេងៗ។</li>
          <li>Hatch - ពួកយើងនឹងជ្រើសរើសសិស្ស និស្សិត ចំនួន ១២០ រូបពីពាក្យស្នើរសុំ ដើម្បីឲ្យមកចូលរួមកម្មវិធី​ Hatch
            ដើម្បីធ្វើបទបង្ហាញពីគំនិតរបស់ពួកគេ និងបង្កើតក្រុមបើសិនគេមិនទាន់មានក្រុម។</li>
          <li>Digithon - ក្រុមនីមួយៗនឹងបានបណ្ដុះបណ្ដាលអំពីបច្ចេកវិទ្យា Internet of Things and កម្មវិធី Software
            ហើយនឹងទទួលបានបទពិសោធន៍
            ក្នុងការបង្កើតផលិតផលគម្រូ។</li>
          <li>Technopreneurship Challenge - និសិ្សត ចំនួន ៦០អ្នក នឹងរៀនពីការអភិវឌ្ឍន៍គំនិត
            និងការបង្កើតក្រុមហ៊ុនបច្ចេកវិទ្យាថ្មីដោយទទួលបានបង្ហាត់បង្រៀនពីសំណាក់វាគ្មិនក្នុងរយៈពេល ៥ថ្ងៃ។</li>
          <li>Final Pitch - កម្មវិធីធ្វើបទបង្ហាញជាសាធារណៈមួយនៅទីក្រុងភ្នំពេញ ដើម្បីជ្រើសរើសក្រុមចំនួន ៥
            ក្រុមដែលមានគំនិតល្អជាងគេ។ ក្រុមទទួលបានជ័យជំនះទាំង៥ នេះនឹងបន្តចូលទៅក្នុងកម្មវិធីបណ្តុះបណ្តាលរយៈពេល ៣
            ខែបន្តទៀត។
          </li>
          <li>The Grand Final Pitch - ក្រុមទទួលបានជ័យជំនះទាំង៥នឹងធ្វើបទបង្ហាញដើម្បីប្រកួតយកសាច់ប្រាក់ 10,000
            ដុល្លារអាមេរិក និងឱកាសចូលរួមទស្សនកិច្ចសិក្សានៅតំបន់អាសុីអាគ្នេយ៍</li>
        </ol>
        </p>
      </div>
    </div>
  </div>

  <div class="section border process">
    <div class="title-line"></div>
    <p class="section-title">ដំណើរការកម្មវិធី</p>
    <p class="side-section-title right">
      ដំណើរការកម្មវិធី
    </p>
    <div id="process-ctn" class="ctn">
      <div class="item">
        <div class="image" style="background-image: url('/images/yip/inspire.svg')"></div>
        <p class="title">វគ្គបំផុសគំនិត</p>
        <p class="description">យើងនឹងជ្រើសរើសសិស្ស និស្សិត ចំនួន ១២០ រូបពីពាក្យស្នើរសុំ ដើម្បីឲ្យមកចូលរួមកម្មវិធី...</p>
        <div class="btn" onclick="openProcessCard('inspire')">
          <p class="text">ព័ត៌មានបន្ថែម</p>
          <div class="icon more"></div>
        </div>
      </div>

      <div class="item white">
        <div class="image" style="background-image: url('/images/yip/enable.svg')"></div>
        <p class="title">វគ្គបណ្តុះសមត្ថភាព</p>
        <p class="description">កម្មវិធីការប្រកួតសហគ្រិនបច្ចេកវិទ្យា (Technoprenuer Challenge)
          ជាកម្មវិធីរយៈពេលប្រាំថ្ងៃ...</p>
        <div class="btn" onclick="openProcessCard('enable')">
          <p class="text">ព័ត៌មានបន្ថែម</p>
          <div class="icon more gray"></div>
        </div>
      </div>

      <div class="item">
        <div class="image" style="background-image: url('/images/yip/grow.svg')"></div>
        <p class="title">វគ្គលូតលាស់</p>
        <p class="description">ទទួលបានបទពិសោធន៍ការងារជាមួយអាជីវកម្មដែលឈានមុខគេនៅក្នុង</p>
        <div class="btn" onclick="openProcessCard('grow')">
          <p class="text">ព័ត៌មានបន្ថែម</p>
          <div class="icon more"></div>
        </div>
      </div>
    </div>



    <div class="slider-btns">
      <div class="btn left" onclick="processSlider(-1)">
        <div class="icon"></div>
        <p class="label">ឆ្វេង</p>
      </div>
      <div class="btn right" onclick="processSlider(1)">
        <p class="label">ស្តាំ</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>

  <div id="requirements" class="section border requirements">
    <div class="title-line"></div>
    <p class="section-title">តើយើងស្វែងរកអ្នកណា?</p>
    <p class="side-section-title left">
      តើយើងស្វែងរកអ្នកណា?
    </p>
    <p class="small-section-title">គ្រប់និស្សិត ដែលមានចំណាប់អារម្មណ៍ និងចង់ស្វែងយល់ពីការច្នៃប្រឌិត សហគ្រិនភាព
      ការបង្កើតជំនួញថ្មី និងបច្ចេកវិទ្យាឌីជីថល។ និស្សិតទាំងនេះត្រូវមានគំនិតជំនួញឌីជីថលដែលមាននៅក្នុងប្រភេទ៖</p>
    <div class="ctn mobile" style="background-image: url('/images/hse/benefits-mobile.png')">
      <div class="item tl">ការអប់រំតាមរយៈឌីជីថល</div>
      <div class="item tr">ការកំសាន្ត និងពត៌មានឌីជីថល</div>
      <div class="item bl">វិស័យផ្សេងទៀត</div>
      <div class="item br">ពាណិជ្ជកម្ម និងការបង់ប្រាក់បែបឌីជីថល</div>
    </div>

    <div class="ctn web" style="background-image: url('/images/hse/benefits.png')">
      <div class="item tl">ការអប់រំតាមរយៈឌីជីថល</div>
      <div class="item tr">ការកំសាន្ត និងពត៌មានឌីជីថល</div>
      <div class="item bl">វិស័យផ្សេងទៀត</div>
      <div class="item br">ពាណិជ្ជកម្ម និងការបង់ប្រាក់បែបឌីជីថល</div>
    </div>
  </div>

  <div id="criteria" class="section border apply">
    <div class="content-ctn">
      <p class="section-title">តើនរណាអាចដាក់ពាក្យបាន?</p>
      <p class="small-section-title">យ៉ាងហោចណាស់ បេក្ខជនត្រូវមាន៖</p>
      <div class="ctn">
        <div class="item">
          <div class="image" style="background-image: url('/images/yip/wca1.svg')"></div>
          <p class="title">ចំនេះដឹងអង់គ្លេសល្អ</p>
        </div>

        <div class="item">
          <div class="image" style="background-image: url('/images/yip/wca2.svg')"></div>
          <p class="title">ចំនង់​និងការប្ដេជ្ញាចិត្តដ៏រឹងមាំ</p>
        </div>

        <div class="item">
          <div class="image" style="background-image: url('/images/yip/wca3.svg')"></div>
          <p class="title">ជំនាញសហគ្រិនភាព​</p>
        </div>
      </div>
    </div>
  </div>

  <div id="previous" class="section border awardee">
    <div class="title-line"></div>
    <p class="section-title">អ្នកឈ្នះជំនាន់មុនៗ</p>
    <div id="awardee-ctn" class="content-ctn">
      <div class="item">
        <p class="yellow-title">Cycle 1</p>
        <div class="ctn">
          <div class="awardee" style="background-image: url('/images/yip/awardees/GoSoccer.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Ligo.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Prestige.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Propey.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Spare.png')"></div>
        </div>
      </div>
      <div class="item">
        <p class="yellow-title">Cycle 2</p>
        <div class="ctn">
          <div class="awardee" style="background-image: url('/images/yip/awardees/Haystome.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/HomeX.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Malis.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/SomJot.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Tosrean.png')"></div>
        </div>
      </div>
      <div class="item">
        <p class="yellow-title">Cycle 3</p>
        <div class="ctn">
          <div class="awardee" style="background-image: url('/images/yip/awardees/RenTech.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/PhumCake.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/SersJbong.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Speakout.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/Style.png')"></div>
        </div>
      </div>
      <div class="item">
        <p class="yellow-title">Cycle 4</p>
        <div class="ctn">
          <div class="awardee" style="background-image: url('/images/yip/awardees/Adero.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/FlexFloc.png')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/iCompost.jpg')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/KELA.jpg')"></div>
          <div class="awardee" style="background-image: url('/images/yip/awardees/YourSpace.png')"></div>
        </div>
      </div>
    </div>

    <div class="slider-btns yellow">
      <div class="btn left" onclick="awardeeSlider(-1)">
        <div class="icon"></div>
        <p class="label">ឆ្វេង</p>
      </div>
      <div class="btn right" onclick="awardeeSlider(1)">
        <p class="label">ស្តាំ</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>

  <div id="faq" class="section border faq">
    <div class="content-ctn">
      <div class="section-title">Frequently Asked Questions</div>
      <div id="group-program" class="group">
        <div class="title-row">
          <div class="image" style="background-image: url('/images/yip/program.svg')"></div>
          <p class="title">The Program</p>
        </div>
        <div class="q">1. What is the objective of this program?</div>
        <div class="a">The SmartStart Young Innovator Program is aimed at enabling and empowering young Cambodian
          talents
          to develop their innovative tech and digital ideas with Smart. The goal is to help turn the best concepts into
          actual tech enterprises.</div>
        <div class="q">2. What are the benefits of joining this program?</div>
        <div class="a">
          <p> SmartStart offers a unique learning platform along with mentorship and financial support. Through
            this program, you will get:</p>
          <p><span>Connected: </span>You will be exposed to various elements of Cambodia’s digital startup ecosystem,
            such as experienced mentors, inspirational speakers, co-working spaces, investors and technical support.</p>
          <p><span>Skills: </span>The program consists of business modules taught by experts in their field who have
            trained and mentored successful startups in Cambodia. The Technopreneurship Challenge will take successful
            teams to a province outside Phnom Penh. (This is subject to change upon consideration of the organizing
            committee in light of the Covid-19 pandemic)</p>
          <p><span>Funcding: </span>Five winning teams will be granted a 3-month paid internship with the leading
            digital and tech startups in Cambodia. The winning team of the Grand Final Pitch will receive 10,000 USD as
            well as an array of resources, mentorships and collaborative opportunities. </p>
        </div>
        <div class="q">3. Do I have to bear any costs to participate in this program?</div>
        <div class="a">Smart will cover all program-related expenses, including meals, refreshments and program
          materials. Transportation and accommodation will also be provided for the Technopreneurship Challenge during
          the Enable Phase.</div>
        <div class="q">4. What is the selection process like?</div>
        <div class="a">
          <p class="subtitle">Inspire Phase:</p>
          <p>A total of 120 students will be selected from a pool of applicants to participate in a one-day event called
            Hatch, during which they will pitch their ideas and form their respective teams based on interests and
            skills.
          </p>
          <p>
            Teams will be invited to a two-day Digithon, which will involve prototyping and performing a demonstration.
            After this event, up to 15 ideas/teams will proceed to the next phase for further development.
          </p>

          <p class="subtitle">Enable Phase:</p>
          <p>This phase will see a five-day off-site Technopreneurship Challenge take place. It will be shaped around
            the fundamentals of ideating and growing a digital startup; recognizing, evaluating and exploiting
            opportunities; understanding the importance of rapid prototyping; market validation; and experiencing all
            aspects of business creation. During this phase, there will be high-level digital training and exposure to
            the basics of back-end and front-end software tools.
          </p>
          <p>
            Participants will hear from guest speakers – local and international – and network with mentors and fellow
            entrepreneurs, with a focus on “learning by doing”. The phase will conclude with a public pitching event in
            Phnom Penh after the five-day Bootcamp. At the event, five teams with the most outstanding and promising
            digital ideas will be granted three-month internships with the leading digital and tech startups in Cambodia
            to further develop their business ideas.
          </p>

          <p class="subtitle">Growth Phase:</p>
          <p>In this phase, teams will develop and realize their ideas in a three-month fully paid internship as well as
            receive tailored support in partnership with Impact Hub Phnom Penh. They will turn their startup ideas into
            actual businesses/products through an ecosystem of resources, mentorships, inspiration and collaboration
            with business and tech consultants.
          </p>
          <p>
            Besides having regular check-ins to monitor progress and troubleshoot challenges, the teams will take part
            in demo days and meet with experts and mentors from various companies to receive valuable insights.
          </p>
          <p>
            Teams will enjoy full membership at Impact Hub Phnom Penh, allowing them full-time use of facilities as well
            as free access to regular events, workshops and business clinics.
          </p>
        </div>
        <div class="q">5. What are the requirements for participation if I am selected?</div>
        <div class="a">If you are selected and continually progress, you will be required to fully participate in all
          events of this program, which is divided into three phases and includes the Hatch, Digithon ,
          Technopreneurship Bootcamp and respective pitching events, as outlined above.</div>
        <div class="group-end" onclick="expandFAQ('program')">
          <div class="image"></div>
        </div>
      </div>
      <div id="group-application" class="group">
        <div class="title-row">
          <div class="image" style="background-image: url('/images/yip/application.svg')"></div>
          <p class="title">The Application</p>
        </div>
        <div class="q">1. When is the application deadline?</div>
        <div class="a">16<sup>th</sup> May, 2022.</div>
        <div class="q">2. Who can apply?</div>
        <div class="a">
          <p>This program is open to all university students (with those in the third or fourth year of their studies
            especially encouraged to apply) with:</p>
          <ul>
            <li>A keen interest in innovation, entrepreneurship, startups and digital technologies.</li>
            <li>Any digital business idea in the following verticals: digital education, digital commerce and payments,
              digital entertainment and content, and other disruptive industry models (in the areas of healthcare,
              agriculture or transportation).</li>
            <li>Digital or entrepreneurial skills that may contribute towards the development of one’s ideas. For
              example, coding, graphic/web design, marketing, writing or sales.</li>
            <li>Good English proficiency.</li>
            <li>A strong drive and determination to make their dreams come true.</li>
            <li>A commitment to fully participate in all sessions of the program.</li>
          </ul>
        </div>
        <div class="q">3. How do I apply to this program?</div>
        <div class="a">
          We only accept online applications. <a href="https://form.typeform.com/to/fC6DaSU1" target="_blank">Click
            here</a> if you’d like to get started on your
          application. The deadline
          for applications is 16<sup>th</sup> May, 2022.
        </div>
        <div class="q">4. What supporting materials are required for the application?</div>
        <div class="a">You are required to submit a video introduction about yourself (please refer to the application
          form for details regarding video content). You must upload your video to YouTube or provide a Google Drive
          link accordingly in the application form. Make sure to double check the sharing options so the link is
          accessible.
        </div>
        <div class="q">5. When and how will I be notified of my admission status?</div>
        <div class="a">You will be informed of your application’s status by the organizers via email. Successful
          applicants will be asked to confirm their participation upon receiving the notification. Failure to do so will
          imply that the applicant has rejected the opportunity to join the program, and the position will be offered to
          other applicants.</div>
        <div class="q">6. Can I apply as an individual or do I need to have a team?</div>
        <div class="a">You can apply to SmartStart as an individual, or as part of a team.If you apply as an individual,
          you can apply with or without an idea. If you apply as a team, you must apply with an idea. If you apply as a
          team, we will either accept your idea and your whole team, or we may reject your idea but consider each
          applicant individually.</div>
        <div class="q">7. Can I apply without an idea?</div>
        <div class="a">You can apply to SmartStart without an idea only if you are applying as an individual.</div>
        <div class="q">8. How do I find a good idea?</div>
        <div class="a">A great idea is something that solves a problem that someone is facing. You can think about a
          problem that you, your family members or friends used to face. A good business idea could also be an
          enterprise that exists online or in another country that you think should exist here. Another suggestion is to
          think about what you’re passionate about and how you can turn that passion into a business. </div>
        <div class="q">9. I’m stuck with my application -- help!</div>
        <div class="a">
          <p>Here are some of our suggestions:</p>
          <ul>
            <li>Make sure your problem is real, specific enough and has a real market with potential customers. Make
              sure you have spoken to people who experience the problem or experts who are working in that area for
              validation! </li>
            <li>You should think outside of the box and come up with solutions that are innovative and creative. We
              recommend you to do some research on what has been done, and how you can make those solutions better.
            </li>
            <li>Make sure to have some data, facts and figures on how big the problem is. </li>
            <li>Lastly, make sure the problem is something you and your team are passionate about -- but also make sure
              your team has the right skills and experience to pull it off. </li>
          </ul>
        </div>
        <div class="q">10. How do I know my team is the right fit for the program?</div>
        <div class="a">
          <p>See this checklist. Your team has: </p>
          <ul>
            <li>People with diverse and complementary skills and backgrounds (at least one person with creative, one
              with business and one with tech skills).</li>
            <li>At least more than half of the team is very passionate about the topic.</li>
            <li>You have the time to commit to this project and you are committed.</li>
            <li>You have the same vision. </li>
            <li>At least one of the team members has some industry insights.</li>
          </ul>
        </div>
        <div class="q">What are the important dates?</div>
        <div class="a">
          <p><span>Hatch: </span> May 29<sup>th</sup>, 2022</p>
          <p><span>Digithon: </span> June 03<sup>rd</sup> - 05<sup>th</sup>, 2022</p>
          <p><span>Technopreneurship Challenge: </span> June 14<sup>th</sup> - 19<sup>th</sup>, 2022</p>
          <p><span>Final Pitch: </span> July 10<sup>th</sup>, 2022</p>
          <p><span>Internship: </span> August - October, 2022</p>
          <p><span>Grand Final Pitch: </span> November 12<sup>th</sup>, 2022</p>
          <p>The organizers reserve the rights to change the dates. In conducting this program, the organizers will
            closely monitor the situation of the spread of coronavirus and will shift arrangements following the
            standard operating procedures issued by the Ministry of Health.
          </p>
        </div>
        <div class="group-end" onclick="expandFAQ('application')">
          <div class="image"></div>
        </div>
      </div>
    </div>
  </div>

  <div id="apply" class="section apply-now"
    style="background: linear-gradient( rgba(0, 154, 62, 1),rgba(0, 154, 62, 0), rgba(0, 154, 62, 0) ), url('/images/client/p-7.jpg')">
    <p class="title">តើអ្នកត្រៀមខ្លួនដើម្បីច្នៃប្រឌិតថ្មីជាមួយSmart?</p>

    <div class="btn" style="background-image: url('/images/client/sim-cut-green.png')"
      onclick="window.open('https://form.typeform.com/to/fC6DaSU1')">
      <p class="text">ដាក់ពាក្យ​ឥឡូវនេះ</p>
      <div class="icon" style="background-image: url('/images/client/right-white.png')"></div>
    </div>
  </div>

  <div class="section partnership">
    <div class="title-line"></div>
    <p class="section-title">ក្នុងភាពជាដៃគូជាមួយ</p>
    <div class="ctn">
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/client/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/client/CBRD.png')"></div>
      </div>
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/Impact.png')"></div>
      </div>
      <div class="item">
        <div class="logo smart" style="background-image: url('/images/client/Smart.png')"></div>
      </div>
    </div>

    <div class="tagline-ctn">
      <div class="tagline">Innovating With You, Investing In You!</div>
      {{-- <div class="logo" style="background-image: url('/images/yip/yip.png')"></div> --}}
    </div>
  </div>


  <div id="enable-card" class="card enable">
    <div class="backdrop" onclick="closeProcessCard()"></div>
    <div class="content-ctn">
      <div class="btn close" onclick="closeProcessCard()"></div>
      <div class="image" style="background-image: url('/images/yip/enable.svg')"></div>
      <div class="content">
        <p class="title">វគ្គបណ្តុះសមត្ថភាព</p>

        <p class="text">កម្មវិធីការប្រកួតសហគ្រិនបច្ចេកវិទ្យា (Technoprenuer Challenge) ជាកម្មវិធីរយៈពេលប្រាំថ្ងៃ
          និងការបង្កើតក្រុមហ៊ុនបច្ចេកវិទ្យាថ្មីដែលរួមមាន៖</p>

        <ol>
          <li>ការចេះវិភាគ និងចេះប្រើប្រាស់ឱកាស</li>
          <li>យល់ដឹងពីសារៈសំខាន់នៃការបង្កើតផលិតផលគម្រូឲ្យបានរហ័ស
          </li>
          <li>យល់ដឹងសុពលភាពទីផ្សារ និងបទពិសោធន៍គ្រប់ផ្នែកនៃការបង្កើតជំនួញមួយ</li>
          <li>ការចូលរួមបង្ហាត់បង្រៀនពីសំណាក់វាគ្មិន ការបង្កើតទំនាក់ទំនងជាមួយអ្នកមានបទពិសោធន៍ និង សហគ្រិនផ្សេងៗ
            ដោយផ្តោតសំខាន់ទៅលើ ការរៀនតាមរយៈការធ្វើ។ នេះរួមមាន ការហ្វឹកហាត់ឌីជីថលកម្រិតខ្ពស់
            និងការរៀនសូត្រពីភាសាសាមញ្ញក្នុងការប្រើប្រាស់សហ្វវែកុំព្យូទ័រមួយចំនួន</li>
        </ol>

        <p class="text">
          វគ្គនេះនឹងបញ្ចប់ជាមួយនឹងកម្មវិធីធ្វើបទបង្ហាញជាសាធារណៈមួយនៅទីក្រុងភ្នំពេញ ដើម្បីជ្រើសរើសក្រុមចំនួន ៥
          ក្រុមដែលមានគំនិតល្អជាងគេ។ ក្រុមទទួលបានជ័យជំនះទាំង៥ នេះនឹងបន្តកម្មសិក្សារយៈពេល ៣
          ខែបន្តទៀតជាមួយក្រុមហ៊ុនបច្ចេកវិទ្យាឈានមុខគេនៅប្រទេសកម្ពុជា។
        </p>
      </div>
    </div>
  </div>

  <div id="inspire-card" class="card inspire">
    <div class="backdrop" onclick="closeProcessCard()"></div>
    <div class="content-ctn">
      <div class="btn close" onclick="closeProcessCard()"></div>
      <div class="image" style="background-image: url('/images/yip/inspire.svg')"></div>
      <div class="content">
        <p class="title">វគ្គបំផុសគំនិត</p>
        <p class="text">យើងនឹងជ្រើសរើសសិស្ស និស្សិត ចំនួន ១២០ រូបពីពាក្យស្នើរសុំ
          ដើម្បីឲ្យមកចូលរួមកម្មវិធីដែលមានរយៈពេលពាក់កណ្តាលថ្ងៃ មានឈ្មោះថា Hatch ។</p>
        <p class="text">បេក្ខជនដែលមានគំនិតអាជីវកម្មជាក់លាក់ នឹងត្រូវបានអញ្ជើញឲ្យធ្វើបទបង្ហាញពីគំនិតរបស់ពួកគេ
          បន្ទាប់មកបេក្ខជននឹងជ្រើសរើសក្រុមដោយផ្អែកទៅលើចំណាប់អារម្មណ៍ និងជំនាញរបស់ពួកគេ។</p>
        <p class="text">ក្រុមនីមួយៗនឹងត្រូវ ចូលរួមកម្មវិធីបង្កើតផលិតផលគម្រូ ដែលមាន រយៈពេល ២ថ្ងៃ ដែលក្នុងនោះពួក
          គេត្រូវធ្វើបទបង្ហាញទៅកាន់គណៈ កម្មការអំពីផលិតផលរបស់ពួកគេ។ អ្នកជំនាញនឹងជ្រើសរើស១៥ ក្រុមដែលល្អជាងគេដើម្បីបន្ត
          ទៅវគ្គបន្ទាប់ដែលមានឈ្មោះថា Technoprenuer Challenge ដែលមានរយៈពេល៥ ថ្ងៃ។</p>
      </div>
    </div>
  </div>

  <div id="grow-card" class="card grow">
    <div class="backdrop" onclick="closeProcessCard()"></div>
    <div class="content-ctn">
      <div class="btn close" onclick="closeProcessCard()"></div>
      <div class="image" style="background-image: url('/images/yip/grow.svg')"></div>
      <div class="content">
        <p class="title">វគ្គលូតលាស់</p>
        <p class="text">ទទួលបានបទពិសោធន៍ការងារជាមួយអាជីវកម្មដែលឈានមុខគេនៅក្នុងប្រទេសកម្ព្ជជា។ នៅពេលបញ្ចប់កម្មសិក្សា
          ក្រុមមួយនឹងមានឱកាសឈ្នះ 10,000 ដុល្លារ។ </p>
      </div>
    </div>
  </div>
</div>
@endsection
