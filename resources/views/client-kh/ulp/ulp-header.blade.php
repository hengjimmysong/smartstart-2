<div class="header ulp-header" data-scroll-pos="0">
  <div class="logo" style="background-image: url('/images/ulp/ulp.png')" onclick="customScrollTo('top')"></div>
  <div id="menu" class="menu ulp">
    <div id="h-home" class="btn item" onclick="customScrollTo('top')" data-scrolled-passed="true">ទំព័រដើម</div>
    <div id="h-about" class="btn item" onclick="customScrollTo('about')">អំពីយើង</div>
    <div id="h-objective" class="btn item" onclick="customScrollTo('objective')">គោលបំណង</div>
    <div id="h-testimonials" class="btn item" onclick="customScrollTo('testimonials')">សក្ខីកម្ម</div>
    <div class="btn item highlight" onclick="window.open('https://ulp.smartstart.com.kh/sign-in')">ULP Site</div>
    <div id="h-lang" class="btn item lang" onclick="window.location.href = '/ulp'"
      style="background-image: url('/images/client/kh.png')"></div>
  </div>
  <div class="menu-btn" onclick="toggleNavBar()"></div>
</div>
