@extends('layouts.ulp.ulp-layout-kh')
@section('header')
<title>SmartStart - ULP</title>
@endsection

@section('content')
<div class="floating-btn kh left" onclick="window.location.href = '/kh'">
  <p class="label">ត្រលប់ទៅទំព័រដើម</p>
  <div class="icon" style="background-image: url('/images/client/left-white.png')"></div>
</div>

<div class="floating-btn kh right" onclick="customScrollTo('top')">
  <div class="icon" style="background-image: url('/images/client/up-white.png')"></div>
  <p class="label">ត្រលប់ទៅខាងលើ</p>
</div>

<div class="ulp-pages ulp-home kh">
  <div class="section cover"
    style="background:   linear-gradient( rgba(0, 154, 62, 1),rgba(0, 154, 62, 1),rgba(0, 154, 62, 1),rgba(0, 154, 62, 1), rgba(0, 154, 62, 0.2))">
    <div class="background"></div>
    <div class="ctn">
      {{-- <p class="title">Welcome to</p> --}}
      <div class="logo" style="background-image: url('/images/client/logo-white.png')"></div>
      <p class="subtitle">Unipreneur Learning Platform</p>
      <div class="divider"></div>
      <p class="text">
        SmartStart Unipreneur Learning Platform គឺជាថ្នាលបណ្តុះបណ្តាលអប់រំរវាង ក្រុមហ៊ុន ស្មាត អាស្យាតា និង អ៊ីមផេកហាប់
        ភ្នំពេញ ។ វាគឺជាទម្រង់នៃការរៀនតាមបែបអនឡាញដំបូងគេបំផុតនៅប្រទេសកម្ពុជា
        ដែលផ្តល់នូវចំណេះផ្នែកសហគ្រិនភាពតាមរយៈវិធីសាស្ត្រនៃការរៀនបែបក្នុងថ្នាក់ផង តាមបែបអនឡាញផង ទៅដល់សិស្សានុសិស្ស
        ក្នុងភាសាខ្មែរ និងអង់គ្លេស ។
        <br><br>
        តាមរយៈការបង្កើតឡើងក្នុងប្រទេសកម្ពុជា​ គោលដៅនៃទម្រង់ការរៀនក៏ដូចជាកម្មវិធីសិក្សាគឺ
        ដើម្បីបង្កើតសមត្ថភាពនិងជំនាញផ្នែកសហគ្រិនភាពក៏ដូចជា អាជីវកម្ម
        និងផ្នត់គំនិតជាសហគ្រិនរបស់សិស្សានុសិស្សតាមរយៈការស្វែងយល់ពីទស្សនៈទានទាំងអស់នេះ ។
        <br><br>
        គំនិតផ្តួចផ្តើមនេះអាចធ្វើទៅបានដោយមានការចូលរួមចំណែកពីក្រុមហ៊ុន ស្មាត អាស្យាតា
        ចំពោះការផ្តល់ថវិកាដល់មូលនិធិនៃការកសាងសមត្ថភាពនិងការស្រាវជ្រាវ និងការសហការជាមួយនឹងក្រសួងប្រៃសណីយ៏ និងទូរគមនាគមន៏
        ។

      </p>
    </div>
  </div>

  <div id="about" class="section border about">
    <div class="title-line"></div>
    <p class="section-title">អំពីយើង</p>
    <div class="ctn">
      <div class="image methodology" style="background-image: url('/images/ulp/methodology.svg')">

      </div>
      <div class="info">
        <div class="title">វិធីសាស្រ្ត</div>
        <p class="text">
          SmartStart Unipreneur Learning Platform គឺជាការរៀនសូត្រតាមបែបលាយបញ្ចូលគ្នា (រៀននៅក្នុងថ្នាក់ផង រៀនតាមអនឡាញផង)
          ទៅដល់សិស្សសាកលវិទ្យាល័យក្នុងប្រទេសកម្ពុជា ដែលខ្លឹមសារមេរៀនមានទាំងភាសាខ្មែរ និងភាសាអង់គ្លេស ។
          <br><br>
          ប្រព័ន្ធគ្រប់គ្រងនៃការរៀនសូត្រនេះបានផ្តល់នៅកម្មវិធីសិក្សាផ្នែកសហគ្រិនភាពដែលលាយបញ្ចូលគ្នាទាំង តាមបែបអនឡាញ
          និងរៀនដោយផ្ទាល់ ដោយផ្តល់អោយសិស្សសាកលវិទ្យាល័យនូវកម្មវិធីសិក្សាដែលមានលក្ខណៈថ្មីៗ
          រួមបញ្ចូលទាំងការលាយឡំគ្នាជាមួយទ្រឹស្តីអាជីវកម្មបែបសកល
          ក៏ដូចជាឧទាហរណ៏ផ្សេងៗដែលបានមកពីអ្នកជោគជ័យនានាក្នុងប្រទេសកម្ពុជា ។
          <br><br>
          ដើម្បីអនុញ្ញាតិអោយសិស្សទទួលបាននូវការយល់ដឹងយ៉ាងស៊ីជម្រៅ កម្មវិធីសិក្សាបានដាក់បញ្ចូលនូវអត្ថបទ វីដេអូ សំនួរខ្លីៗ
          និងសកម្មភាពផ្សេងៗមួយចំនួនដែលអាចអោយសិស្សទទួលបានការសិក្សាតាមបែបរស់រវើក
          និងផ្សាភ្ជាប់ជាមួយនឹងបទពិសោធន៏របស់សិស្សសកលវិទ្យាល័យ ។ ជាឧទាហរណ៏ សកម្មភាពតាមរយៈអនឡាញដែលតម្រូវអោយសិស្សបង្កើតនូវ
          canvas ក៏ដូចជាមានទំនាក់ទំនង និងធ្វើការជាមួយគ្នា
          ចំនែកឯសកម្មភាពដោយផ្ទាល់គឺទាមទារអោយសិស្សអនុវត្តការងារក្រៅថ្នាក់ដើម្បី រៀនសូត្រតាមរយៈការធ្វើ ។
        </p>
      </div>
    </div>

    <div class="ctn reverse">
      <div class="image coursesyllabus" style="background-image: url('/images/ulp/coursesyllabus.svg')">

      </div>
      <div class="info">
        <div class="title">កម្មវិធីសិក្សា</div>
        <p class="text">
          ១. ក្នុងអំឡុងពេលដំណាក់កាល ' Design Thinking ' និស្សិតនឹងបង្កើតគំនិតថ្មី ។ ដំបូងនិសិ្សតនឹងចំណាយពេលសម្ភាសន៍
          'អតិថិជន' ដើម្បីយល់ពីបញ្ហារបស់ ពួកគេ និងបង្កើនការយល់ដឹងអំពីអតិថិជន ។ បន្ទាប់មកពួកគេ កំណត់បញ្ហាអតិថិជន
          ដើម្បីដោះស្រាយតាមទស្សនៈរបស់អតិថិជន និងទាក់ទងនឹងតម្រូវការរបស់អតិថិជន ។
          <br>
          <br>
          ២. សិស្សរៀនតាមរយៈ «ការធ្វើតាមប្រតិបត្តិ» ក្នុងដំណាក់កាលនៃ 'Lean Startup' ។ ពួកគេវិភាគ
          គំនិតរបស់ពួកគេដើម្បីកំណត់ការសន្មតសំខាន់ៗ។ បន្ទាប់មកពួកគេបង្កើត Prototype នៃ ផលិតផល ឬ
          សេវាកម្មរបស់ពួកគេដើម្បីធ្វើតេស្តតាមការសន្មត ។ Prototype ត្រូវបានអតិថិជនសាកល្បងប្រើប្រាស់
          ដើម្បីទទួលយកនូវមតិយោបល់របស់ពួកគេត្រលប់មកវិញ ។ និស្សិតត្រូវបានលើកទឹកចិត្តឲ្យរៀនពី 'ភាពបរាជ័យ' និង
          ប្រើព័ត៌មានដែលពួកគេទទួលបាន ដើម្បីធ្វើឲ្យកាន់តែប្រសើរឡើង។ សិស្សទទួលបានជំនាញក្នុងការវិភាគដ៏សំខាន់និង
          កសាងទំនុកចិត្តឲ្យកាន់តែសកម្មនិង ហ៊ានប្រថុយប្រថាន ។
          <br>
          <br>
          ៣. សិស្សរៀនមុខងារសំខាន់ៗនៃគំរូអាជីវកម្មតាមរយៈដំណាក់កាល 'Business Model Canvas' ។
          ពួកគេរៀនពីរបៀបបង្កើតគំរូអាជីវកម្មដែលអាចដំណើរការ បាន ប្រកបដោយនិរន្តរ៍ភាព តាមរយៈ ផ្នែកចំនួន 9 នៅក្នុង BMC ។
          កម្មវិធីសិក្សាបញ្ចប់ដោយមានការធ្វើបទបង្ហាញផលិតផលឬសេវាកម្ម និង BMC របស់ ពួកគេទៅកាន់អ្នកនៅក្នងថ្នាក់គេ។
          <br><br>
          កម្មវិធីសិក្សានេះមានរយៈពេល៖
        <ul>
          <li>១៥ សប្តាហ៏</li>
          <li>៤៥ម៉ោង នៃការធ្វើការងារ</li>
          <li>៣០ម៉ោង ក្នុងថ្នាក់រៀន និង ១៥ម៉ោងនៅខាងក្រៅថ្នាក់រៀន</li>
        </ul>
        </p>
      </div>
    </div>
  </div>

  <div id="objective" class="section border objective">
    <div class="title-line"></div>
    <p class="section-title">គោលបំណង</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/ulp/objective.svg')">
      </div>
      <div class="info">
        <p class="text">
          តាមរយៈការឆ្លងកាត់វគ្គសិក្សា និស្សិតសាកលវិទ្យាល័យ នឹងទទួលបានការយល់ដឹងស៊ីជម្រៅអំពីសហគ្រិនភាព
          ការអភិវឌ្ឍផ្នត់គំនិតជាសហគ្រិនរបស់ពួកគេ និងបង្កើនសមត្ថភាពការងារ និងជំនាញបំនិនជីវិតរបស់ពួកគេ ។
          ដូច្នេះពួកគេអាចប្រឈមមុខនឹងទីផ្សារក្នុងស្រុក និងឱកាសការងារអនាគតរបស់ពួកគេ
          ហើយទទួលបានការបំផុសគំនិតពីអ្នកដឹកនាំអាជីវកម្មក្នុងស្រុក ។
          <br><br>
          វេទិកានេះ គឺមានតម្លៃសម្រាប់និស្សិតសាកលវិទ្យាល័យទាំងអស់ -
          ទោះបីជាពួកគេមិនចង់ឬក៏មិនអាចចាប់ផ្តើមអាជីវកម្មផ្ទាល់ខ្លួន - ដោយសារការមានផ្នត់គំនិត ជាសហគ្រិន
          គឺជាជំនាញក្នុងជីវិតដ៏មានតម្លៃមួយសម្រាប់ការអភិវឌ្ឍខ្លួននិងវិជ្ជាជីវៈរបស់ខ្លួន ។
          <br><br>
          គោលបំណងដែលវេទិកានេះគឺដើម្បីឱ្យយុវជនមានជំនាញចំណេះដឹងនិងជំរុញទឹកចិត្តឲ្យក្លាយជាសមាជិកដ៏មានតម្លៃរបស់សហគមន៍
          នៅពេលដែលពួកគេបញ្ចប់ការសិក្សា ។

        </p>
      </div>
    </div>
  </div>

  <div id="testimonials" class="section border testimonials">
    <div class="title-line"></div>
    <p class="section-title">សក្ខីកម្មរបស់និស្សិតបញ្ចប់ការសិក្សា</p>
    <div class="ctn">
      <div class="image-ctn">

        <div class="border-ctn">
          <div class="image" style="background-image: url('/images/ulp/t1.jpeg')"></div>
        </div>
      </div>

      <div class="info">
        <p class="name"><span>ម៊ុុយ</span> ងុិង</p>
        <p class="text">
          “ខ្ញុំបានរៀនពីភាពខុសគ្នារវាងអាជីវកម្ម និងសហគ្រិនភាព។ វាបានជួយខ្ញុំឱ្យបង្កើនការយល់ដឹងរបស់ខ្ញុំអំពីបញ្ហា
          និងឱកាសក្នុងពិភពលោក និងរបៀបដែលយើងអាចក្លាយជាផ្នែកមួយនៃដំណោះស្រាយ។ ឥឡូវនេះ ខ្ញុំដឹងថាពិភពលោកធំប៉ុនណា
          ក៏ដូចជាបញ្ហាមួយចំនួនដែលយើងត្រូវដោះស្រាយ។”
        </p>
      </div>
    </div>

    <div class="ctn reverse">
      <div class="image-ctn">

        <div class="border-ctn">
          <div class="image" style="background-image: url('/images/ulp/t2.jpeg')"></div>
        </div>
      </div>

      <div class="info">
        <p class="name"><span>យ៉ង់សុខ</span> ចំរើន</p>
        <p class="text">
          “សារ៉ាត់ [សាស្ត្រាចារ្យនៅ RULE] បាននិយាយថា យើងមិនមែនគ្រាន់តែទេត្រូវតែមានផ្នត់គំនិតសហគ្រិន
          សម្រាប់ធ្វើអាជីវកម្មទេ; យើងពិតជាត្រូវការវាដើម្បីជោគជ័យក្នុងជីវិតផងដែរ។ ខ្ញុំពិតជាចូលចិត្តវគ្គសិក្សានេះ។
          វាបានផ្លាស់ប្តូរទស្សនៈរបស់ខ្ញុំលើការមានផ្នត់គំនិតសហគ្រិន។
          ”

        </p>
      </div>
    </div>
  </div>

  <div id='partner' class="section border partner">
    <div class="title-line"></div>
    <p class="section-title">សាកលវិទ្យាល័យដៃគូ</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/ulp/partneruni.svg')">
      </div>
      <div class="partners">
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u1.png')"></div>
          <div class="text">
            <p>បណ្ឌិត្យសភាបច្ចេកវិទ្យាឌីជីថលកម្ពុជា (CADT)</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u2.png')"></div>
          <div class="text">
            <p>សាកលវិទ្យាល័យពុទ្ធិសាស្រ្ត</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u3.png')"></div>
          <div class="text s">
            <p>សាកលវិទ្យាល័យគ្រប់គ្រងជាតិ</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u4.png')"></div>
          <div class="text s">
            <p>សាកលវិទ្យាល័យភូមិន្ទនីតិសាស្រ្ត និងវិទ្យាសាស្រ្តសេដ្ឋកិច្ច</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u5.png')"></div>
          <div class="text s">
            <p>សាកលវិទ្យាល័យបៀលប្រាយភ្នំពេញ</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u5.png')"></div>
          <div class="text s">
            <p>សាកលវិទ្យាល័យបៀលប្រាយសៀមរាប</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u6.png')"></div>
          <div class="text">
            <p>សាកលវិទ្យាល័យភូមិន្ទកសិកម្ម</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u7.png')"></div>
          <div class="text">
            <p>សកលវិទ្យាល័យ​បច្ចេកវិទ្យា​និង​វិទ្យាសាស្ត្រ​កម្ពុជា​</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u8.png')"></div>
          <div class="text s">
            <p>សាកលវិទ្យាល័យបញ្ញាសាស្រ្តបាត់ដំបង</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u9.png')"></div>
          <div class="text">
            <p>សាកលវិទ្យាល័យអង្គរ</p>
          </div>
        </div>
        <div class="item">
          <div class="logo" style="background-image: url('/images/ulp/u10.png')"></div>
          <div class="text">
            <p>សាកលវិទ្យាល័យស្វាយរៀង</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section partnership">
    <div class="title-line"></div>
    <p class="section-title">ដៃគូសហការ</p>
    <div class="ctn">
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/client/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/client/CBRD.png')"></div>
      </div>
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/Impact.png')"></div>
      </div>
      <div class="item">
        <div class="logo smart" style="background-image: url('/images/client/Smart.png')"></div>
      </div>
    </div>

    {{-- <div class="tagline-ctn">
      <div class="tagline">Innovating With You, Investing In You!</div>
      <div class="logo" style="background-image: url('/images/yip/yip.png')"></div>
    </div> --}}
  </div>
</div>
@endsection
