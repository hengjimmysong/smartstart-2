@extends('layouts.hse.hse-layout-kh')
@section('header')
<title>SmartStart - HSE</title>
@endsection

@section('content')
<div class="floating-btn kh left" onclick="window.location.href = '/kh'">
  <p class="label">ត្រលប់ទៅទំព័រដើម</p>
  <div class="icon" style="background-image: url('/images/client/left-white.png')"></div>
</div>

<div class="floating-btn kh right" onclick="customScrollTo('top')">
  <div class="icon" style="background-image: url('/images/client/up-white.png')"></div>
  <p class="label">ត្រលប់ទៅខាងលើ</p>
</div>

<div class="hse-pages hse-home kh">
  <div class="section cover"
    style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('/images/client/p-4.jpg')">
    <p class="title">
      SmartStart
    </p>
    <div class="description">High School Edition</div>
    <div class="btn" style="background-image: url('/images/client/sim-cut-green.png')"
      onclick="window.open('https://bit.ly/3tmcAMO')">
      <p class="text">ដាក់ពាក្យ​ឥឡូវនេះ</p>
      <div class="icon" style="background-image: url('/images/client/right-white.png')"></div>
    </div>
  </div>
  <div class="section border about-us">
    <p class="subtitle">ទិដ្ឋភាពទូទៅ</p>
    <div class="title-line"></div>
    <p class="section-title">អំពីកម្មវិធី SmartStart High School Edition</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/hse/about-us.png')">

      </div>
      <div class="info">
        <p class="text">
          ដោយផ្អែកលើភាពជោគជ័យដែលទទួលបាននៃកម្មវិធី SmartStart Young Innovator Program, កម្មវិធី SmartStart
          High School Edition គឺបានបង្កើតឡើងដែលមានរយៈពេល២ថ្ងៃ កន្លះ
          ដែលផ្តល់ឱកាសដល់សិស្សវិទ្យាល័យស្វែងយល់អំពី អាជីវកម្មឌីជីថល និងសហគ្រិនភាព។
          <br><br>
          កម្មវិធី SmartStart High School Edition​ ត្រូវបានរៀបចំឡើង ដើម្បីណែនាំសិស្សវិទ្យាល័យនូវមូលដ្ឋានគ្រឹះ
          នៃសហគ្រិនបែបឌីជីថល ដោយមានរួមបញ្ចូលទាំង មេរៀនអាជីវកម្មបែបឌីជីថល សហគ្រិនភាព និងជំនាញទន់សំខាន់ៗ
          ក៏ដូចជាបណ្តុះនូវផ្នត់គំនិតបែបសហគ្រិនទៅដល់សិស្សានុសិស្សផងដែរ។
          <br><br>
          អ្នកចូលរួមត្រូវបានរំពឹងទុកថានឹងទទួលបាននូវការស្វែងយល់និងចំនេះដឹងទូលំទូលាយអំពី ឧបករណ៏
          និងជំនាញនៃអាជីវកម្មតាមរយៈសិក្ខាសាលា, simulation, របៀបនិងបែបបទក្នុងការធ្វើការជាក្រុម, ការបង្វឹក ក៏ដូចជា
          ករណីសិក្សា ក្នុងកម្មវិធីទាំងមូល។
        </p>
      </div>
    </div>
  </div>
  <div id="objectives" class="section border stake">
    <p class="subtitle">គោលបំណង</p>
    <div class="title-line"></div>
    <p class="section-title">លទ្ធផលដែលរំពឹងទុក</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/hse/objective.png')">

      </div>
      <div class="info">
        {{-- <div class="title">Program Structure</div> --}}
        <p class="text">
          មេរៀនទាំងអស់គឺរួមមាន៣ផ្នែកធំៗ ដើម្បីធានានូវការអភិវឌ្ឈន៏ទាំងស្រុងដល់អ្នកចូលរួម។
          ស្របទៅតាមការច្នៃប្រឌិតបែបឌិជីថលក្នុងយុគសម័យរបស់យើង
          កម្មវិធី​​នឹង​ណែនាំ​អ្នកចូលរួមអំពីនិន្នាការបច្ចេកវិទ្យាក្នុងពេលបច្ចុប្បន្ន និងអនាគត។
          ដោយរួមបញ្ចូលជាមួយការយល់ដឹងមូលដ្ឋានគ្រឹះនៃបច្ចេកវិទ្យា
          អ្នកចូលរួមក័៏នឹងទទួលបាននូវគំនិតមូលដ្ឋាននៃសហគ្រិនភាព
          និងថាតើការដែលមានផ្នត់គំនិតបែបសហគ្រិនភាពអាចមានសារៈប្រយោជន៏ក្នុងការឆ្ពោះទៅរកជីវិតនិងរបៀបរស់នៅបែបពេញលេញ។
          តាមរយៈកម្មវិធីនេះ អ្នកចូលរួមនឹងរៀនបាននូវជំនាញទន់សំខាន់ៗដែលនឹងជំរុញការអភិវឌ្ឈន៏ខ្លួនឯង
          និងភាពមានការងារធ្វើនាពេលអនាគត។

        </p>
        <ul>
          <li>ផ្តល់ចំនេះដឹងមូលដ្ឋានពាក់ព័ន្ធទៅនឹង ការរៀបចំផែនការអាជីវកម្ម និង business canvases</li>
          <li>ជំរុញសិស្សអោយចេះប្រើការអង្កេតក្នុងសហគមន៏របស់ពួកគេហើយប្រែក្លាយបញ្ហាដែលពួកគេបានឃើញអោយទៅជាឱកាសអាជីវកម្ម</li>
          <li>ទទួលបានជំនាញទន់ រួមមាន ការទំនាក់ទំនង, ការធ្វើការជាក្រុម និងការនិយាយជាសាធារណៈ</li>
          <li>ទទួលការជំរុញទឹកចិត្ត និង ការស្វែងយល់អោយបានស៊ីជម្រៅ ក្នុងដំណើរនៃសហគ្រិនភាព</li>
          <li>រៀនសូត្រតាមរយៈការធ្វើនូវសកម្មភាពដោយផ្ទាល់</li>
          <li>ទទួលបានមតិយោបល់ត្រលប់ពីគ្រូបង្វឹក</li>
        </ul>
      </div>
    </div>
  </div>
  <div id="structure" class="section border structure">
    <p class="subtitle">រចនាសម្ព័ន្ធ</p>
    <div class="title-line"></div>
    <p class="section-title">អ្វីដែលអ្នកនឹងឆ្លងកាត់?</p>
    <div id="structure-ctn" class="ctn">
      <div class="item">
        <div class="image" style="background-image: url('/images/hse/rocket.png')"></div>
        <p class="title">អាជីវកម្មបែបឌីជីថល</p>
      </div>

      <div class="item">
        <div class="image" style="background-image: url('/images/hse/brain.png')"></div>
        <p class="title">សហគ្រិនភាព</p>
      </div>

      <div class="item">
        <div class="image" style="background-image: url('/images/hse/light-bulb.png')"></div>
        <p class="title">ជំនាញទន់</p>
      </div>
    </div>
    <div class="slider-btns">
      <div class="btn left" onclick="structureSlider(-1)">
        <div class="icon"></div>
        <p class="label">ឆ្វេង</p>
      </div>
      <div class="btn right" onclick="structureSlider(1)">
        <p class="label">ស្តាំ</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>
  <div id="benefits" class="section border benefits">
    <p class="subtitle">អត្ថប្រយោជន៏</p>
    <div class="title-line"></div>
    <p class="section-title">តើអ្នកនឹងទទួលបានអ្វី?</p>
    {{-- <p class="small-section-title">
      សិស្សវិទ្យាល័យទាំងអស់ដែលមានចំណាប់អារម្មណ៍និងចង់ដឹងចង់ឃើញក្នុងការច្នៃប្រឌិតសហគ្រិនភាពការចាប់ផ្តើមអាជីវកម្មនិងបច្ចេកវិទ្យាឌីជីថល។
      និស្សិតទាំងនេះត្រូវតែមានគំនិតអាជីវកម្មឌីជីថលណាមួយនៅខាងក្រោម:
    </p> --}}
    <div class="ctn mobile" style="background-image: url('/images/hse/benefits-mobile.png')">
      <div class="item tl">ទទួលបានឱកាសក្នុងការចូលរួមដំនើរទស្សនកិច្ចទៅកន្លែងអាជីវកម្មនានានៅទីក្រុងភ្នំពេញ</div>
      <div class="item tr">ទទួលបានការយល់ដឹងស៊ីជម្រៅពីចំនេះដឹងមូលដ្ឋាននៃ អាជីវកម្មបែបឌីជីថល និងសហគ្រិនភាព
        សិស្សក៏ទទួលបាននូវការបណ្តុះជំនាញទន់ដែលមានសារៈសំខាន់សម្រាប់ពួកគេនៅថ្ងៃអនាគត
      </div>
      <div class="item bl">ជួបជាមួយសហគ្រិនឆ្នើមៗនានាដើម្បីរៀនសូត្រពីជីវិត និងបទពិសោធន៏ក្នុងភាពជាសហគ្រិន របស់ពួកគាត់
      </div>
      <div class="item br">ទទួលបានឱកាសក្នុងការឈ្នះជាប្រាក់រង្វាន់ដែលសរុបរហូតដល់ ២២០០ដុល្លា។</div>
    </div>

    <div class="ctn web" style="background-image: url('/images/hse/benefits.png')">
      <div class="item tl">ទទួលបានឱកាសក្នុងការចូលរួមដំនើរទស្សនកិច្ចទៅកន្លែងអាជីវកម្មនានានៅទីក្រុងភ្នំពេញ</div>
      <div class="item tr">ទទួលបានការយល់ដឹងស៊ីជម្រៅពីចំនេះដឹងមូលដ្ឋាននៃ អាជីវកម្មបែបឌីជីថល និងសហគ្រិនភាព
        សិស្សក៏ទទួលបាននូវការបណ្តុះជំនាញទន់ដែលមានសារៈសំខាន់សម្រាប់ពួកគេនៅថ្ងៃអនាគត
      </div>
      <div class="item bl">ជួបជាមួយសហគ្រិនឆ្នើមៗនានាដើម្បីរៀនសូត្រពីជីវិត និងបទពិសោធន៏ក្នុងភាពជាសហគ្រិន របស់ពួកគាត់
      </div>
      <div class="item br">ទទួលបានឱកាសក្នុងការឈ្នះជាប្រាក់រង្វាន់ដែលសរុបរហូតដល់ ២២០០ដុល្លា</div>
    </div>
  </div>
  <div id="criteria" class="section border who-can-apply">
    <p class="subtitle">លក្ខន្តិក</p>
    <div class="title-line"></div>
    <p class="section-title">តើយើងកំពុងរកអ្នកណា?</p>
    <p class="small-section-title">ជាអប្បបរមា, បេក្ខជនត្រូវតែជា:</p>
    <div class="ctn">
      <div class="item">
        <div class="image" style="background-image: url('/images/hse/school.png')"></div>
        <div class="divider"></div>
        <p class="title">ជាសិស្សវិទ្យាល័យ<br>នៅថ្នាក់ទី១០ និង ទី១១</p>
      </div>
      <div class="item">
        <div class="image" style="background-image: url('/images/hse/citizen.png')"></div>
        <div class="divider"></div>
        <p class="title">សិស្សដែលកំពុងរស់នៅ <br>ខេត្តកំពង់ចាម, <br>កំពង់ឆ្នាំង, <br>និងកំពង់ធំ</p>
      </div>
      <div class="item">
        <div class="image" style="background-image: url('/images/hse/participation.png')"></div>
        <div class="divider"></div>
        <p class="title">អាចចូលរួមបានពេញលេញ<br>តាមកាលវិភាគកម្មវិធី</p>
      </div>
    </div>
  </div>

  {{-- <div class="section border faq">
    <div class="title-line"></div>
    <p class="section-title">Frequently Asked Questions</p>
    <div id="faq-ctn" class="ctn">
      <div class="item">
        <div class="image"></div>
        <p class="title">សំណួរទូទៅ</p>
        <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div>
      </div>

      <div class="item">
        <div class="image"></div>
        <p class="title">លក្ខន្តិកនៃការដាក់ពាក្យ</p>
        <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div>
      </div>

      <div class="item">
        <div class="image"></div>
        <p class="title">កម្មវិធីសិក្សាដោយខ្លួនឯង</p>
        <div class="btn">
          <p class="text">Learn more</p>
          <div class="icon"></div>
        </div>
      </div>
    </div>
    <div class="slider-btns">
      <div class="btn left" onclick="faqSlider(-1)" style="background-image: url('/images/client/left.png')">
      </div>
      <div class="btn right" onclick="faqSlider(1)" style="background-image: url('/images/client/right.png')">
      </div>
    </div>
  </div> --}}

  <div class="section apply-now"
    style="background: linear-gradient( rgba(0, 154, 62, 1),rgba(0, 154, 62, 0), rgba(0, 154, 62, 0) ), url('/images/client/p-7.jpg')">
    <p class="title">តើអ្នកត្រៀមខ្លួនដើម្បីច្នៃប្រឌិតថ្មីជាមួយSmart?</p>

    <div class="btn" style="background-image: url('/images/client/sim-cut-green.png')"
      onclick="window.open('https://bit.ly/3tmcAMO')">
      <p class="text">ដាក់ពាក្យ​ឥឡូវនេះ</p>
      <div class="icon" style="background-image: url('/images/client/right-white.png')"></div>
    </div>
  </div>

  <div class="section partnership">
    <div class="title-line"></div>
    <p class="section-title">ដៃគូសហការ</p>
    <div class="ctn">
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/client/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/client/CBRD.png')"></div>
      </div>
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MEF.png')"></div>
        <div class="logo KE" style="background-image: url('/images/client/KE.png')"></div>
        <div class="logo smart" style="background-image: url('/images/client/Smart.png')"></div>
      </div>
    </div>

    <div class="tagline-ctn">
      <div class="tagline">Develop Young Entrepreneurs</div>
      {{-- <div class="logo" style="background-image: url('/images/hse/hse.png')"></div> --}}
    </div>
  </div>
</div>
@endsection
