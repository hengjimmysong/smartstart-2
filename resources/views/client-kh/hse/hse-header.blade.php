<div class="header hse-header" data-scroll-pos="0">
  <div class="logo" style="background-image: url('/images/hse/hse.png')" onclick="customScrollTo('top')"></div>
  <div id="menu" class="menu">
    <div id="h-home" class="btn item" onclick="customScrollTo('top')" data-scrolled-passed="true">ទិដ្ឋភាពទូទៅ</div>
    <div id="h-objectives" class="btn item" onclick="customScrollTo('objectives')">គោលបំណង</div>
    <div id="h-structure" class="btn item" onclick="customScrollTo('structure')">រចនាសម្ព័ន្ធ</div>
    <div id="h-benefits" class="btn item" onclick="customScrollTo('benefits')">អត្ថប្រយោជន៍</div>
    <div id="h-criteria" class="btn item" onclick="customScrollTo('criteria')">លក្ខណៈវិនិច្ឆ័យ</div>
    <div id="h-lang" class="btn item lang" onclick="window.location.href = '/hse'"
      style="background-image: url('/images/client/kh.png')"></div>
  </div>
  <div class="menu-btn" onclick="toggleNavBar()"></div>
</div>
