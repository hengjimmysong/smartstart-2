@extends('layouts.main-page.main-page-layout-kh')
@section('header')
<title>SmartStart</title>
@endsection

@section('content')
<div class="m-pages m-home kh">
  <div id="home" class="section cover"
    style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('/images/client/p-1.jpg')">
    <div class="title-img" style="background-image: url('/images/main-page/logo-white.png')"></div>
    <div class="divider"></div>
    <div class="description">ជម្រុញកំនើនសេដ្ឋកិច្ចបែបឌីជីថលរបស់ប្រទេសកម្ពុជានាយុគសម័យឧស្សាហកម្ម ជំនាន់ទី ៤
      តាមរយៈបច្ចេកវិទ្យាប្រកបដោយភាពច្នៃប្រឌិត</div>
  </div>
  <div id="about" class="section about-us">
    <div class="title-line"></div>
    <p class="section-title">អំពីយើង</p>
    <div id="about-us-ctn" class="ctn">
      <div class="item">
        <div class="title">បេសកកម្ម</div>
        <div class="icon" style="background-image: url('/images/main-page/mission.png"></div>
        <div class="divider"></div>
        <div class="desc-ctn">
          <p class="description">
            ផ្តល់អោយយុវជនវ័យក្មេងក្នុងប្រទេសកម្ពុជាជាមួយនឹងជំនាញបែបឌីជីថលនិងសហគ្រិនភាពតាមរយៈបទពិសោធន៏ដ៏សម្បើមក្នុងកម្មវិធី
            Hackathon , Bootcamps និងវគ្គសិក្សាតាមអនឡាញរួមបញ្ចុលគ្នាដែលពួកគេអាចក្លាយទៅជាអ្នកដឹកនាំដែលមានទេញកោសល្យ
            ភាពច្នៃប្រឌិតតាមបែបឌីជីថលសម្រាប់ប្រទេសកម្ពុជា។</p>
        </div>
      </div>

      <div class="item">
        <div class="title">ទស្សនវិស័យ</div>
        <div class="icon" style="background-image: url('/images/main-page/vision.png"></div>
        <div class="divider"></div>
        <div class="desc-ctn">
          <p class="description">តាមរយៈការសហការជាមួយក្រសួងប្រៃសណីយ៏ និង ទូរគមនាគមន៏ និងក្រសួងអប់រំយុវជន និងកីឡា
            ក្រុមហ៊ុនស្មាតអាស្យាតា មានចក្ខុវិស័យអភិវឌ្ឈន៏អ្នកច្នៃប្រឌិតឆ្នើម ដែលនឹងចូល
            រួមចំណែកលើកស្ទួយការរីកចម្រើនឌីជីថលនៅកម្ពុជា ក្នុងយុគសម័យឧស្សាហកម្មជំនាន់ទី៤ តាមរយៈកម្មវិធី SmartStart។ </p>
        </div>
      </div>

      <div class="item">
        <div class="title">ប្រវត្តិនៃការបង្កើត</div>
        <div class="icon" style="background-image: url('/images/main-page/history.png"></div>
        <div class="divider"></div>
        <div class="desc-ctn">
          <p class="description">ចាប់តាំងពីការបង្កើតឡើងតាំងពីឆ្នាំ២០១៧, SmartStart ធ្វើការដើម្បីផ្ដល់អោយសិស្ស​
            សាកលវិទ្យាល័យ​នៅប្រទេសកម្ពុជាចាប់ផ្តើមគំនិតអាជីវកម្មឌីជីថលដោយផ្តល់នូវបទពិសោធន៏នៃការរៀនសូត្របែបថ្មីនិងប្លែកតាមរយៈការបង្ហាត់បង្ហាញ
            និងការគាំទ្រផ្នែកហិរញ្ញវត្ថុ ជាដើម។ SmartStart ​បានពង្រីកនូវកម្មវិធីរបស់ខ្លួន ដោយផ្តល់ជូន នូវការរៀនសូត្រអំពី
            សហគ្រិនភាពដល់យុវជនវ័យក្មេងក្នុងប្រទេសកម្ពុជា​ ហើយបានផ្តល់ផលជាវិជ្ជមានដល់យុវជន ប្រហែល១ពាន់នាក់។</p>
        </div>
      </div>
    </div>
    <div class="slider-btns">
      <div class="btn left" onclick="aboutUsSlider(-1)">
        <div class="icon"></div>
        <p class="label">ឆ្វេង</p>
      </div>
      <div class="btn right" onclick="aboutUsSlider(1)">
        <p class="label">ស្តាំ</p>
        <div class="icon"></div>
      </div>
    </div>
  </div>

  <div id="programs" class="section program">
    <div class="title-line"></div>
    <p class="section-title">កម្មវិធី</p>
    <div class="ctn"
      style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('/images/client/p-3.jpg')">
      <div class="item" onclick="window.location.href = '/kh/yip'">
        <p class="title">SmartStart <br>Young Innovator Program</p>
      </div>
      <div class="item" onclick="window.location.href = '/kh/hse'">
        <p class="title">SmartStart <br>High School Edition</p>
      </div>
      <div class="item" onclick="window.location.href = '/kh/ulp'">
        <p class="title">SmartStart <br>Unipreneur Learning Platform</p>
      </div>
    </div>
  </div>

  <div id="contact" class="section contact-us">
    <div class="title-line"></div>
    <p class="section-title">ទំនាក់ទំនងយើង</p>
    <div class="ctn">
      <div class="item">
        <div class="icon" style="background-image: url('/images/main-page/pin.png')"></div>
        <div class="info">
          {{-- <p class="title">Visit Us</p> --}}
          <p class="value">៤៦៤A មហាវិថីមុនីវង្ស<br>
            សង្កាត់ទនេ្លបាសាក់ ខណ្ឌចំការមន
            <br>ភ្នំពេញ កម្ពុជា
        </div>
      </div>

      <div class="divider"></div>

      <div class="item">
        <div class="icon" style="background-image: url('/images/main-page/phone.png')"></div>
        <div class="info">
          {{-- <p class="title">Call Us</p> --}}
          <p class="value short">+855 (0) 10 234 075</p>
        </div>
      </div>

      <div class="divider"></div>

      <div class="item">
        <div class="icon" style="background-image: url('/images/main-page/mail.png')"></div>
        <div class="info">
          {{-- <p class="title">Email Us</p> --}}
          <p class="value short">sustainability@smart.com.kh</p>
        </div>
      </div>
    </div>
    <div class="map-ctn">
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.197852927056!2d104.92135141494555!3d11.537660347835356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310950dbd81ec78d%3A0x8d3ccf621a2b4e1f!2sSmart+Axiata!5e0!3m2!1sen!2skh!4v1552488128259"
        frameborder="0" style="border:0" allowfullscreen class="frame"></iframe>
    </div>
  </div>
  <div class="section fill-your-info">
    <div class="title-line"></div>
    <p class="section-title">បំពេញព័ត៌មាន</p>
    <div class="ctn">
      <div class="image" style="background-image: url('/images/main-page/form.png')"></div>
      <form class="form" action="/contacts" method="POST">
        {{csrf_field()}}
        <input type="text" name="name" class="input" placeholder="ឈ្មោះរបស់អ្នក" required>
        <input type="text" name="phone" class="input" placeholder="លេខទូស័ព្ទ" required>
        <input type="text" name="email" class="input" placeholder="អុីមែល" required>
        <textarea name="message" class="input text" placeholder="សាររបស់អ្នក" rows="3" required></textarea>
        <input type="text" name="source" style="display: none" value="main">

        <input type="submit" value="Compose" class="btn submit">
      </form>
    </div>
  </div>
  <div class="section partnership">
    <div class="title-line"></div>
    <p class="section-title">ក្នុងភាពជាដៃគូជាមួយ</p>
    <div class="ctn">
      <div class="item">
        <div class="logo" style="background-image: url('/images/client/MoEYS.png')"></div>
        <div class="logo" style="background-image: url('/images/client/MPTC.png')"></div>
        <div class="logo CBRD" style="background-image: url('/images/client/CBRD.png')"></div>
      </div>
      <div class="item">
        <div class="logo smart" style="background-image: url('images/client/Smart.png')"></div>
      </div>
    </div>
  </div>
</div>
@endsection
