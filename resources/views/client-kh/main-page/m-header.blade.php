<div class="header m-header" data-scroll-pos="0">
  <div class="logo" style="background-image: url('/images/main-page/logo.png')" onclick="customScrollTo('top')"></div>
  <div id="menu" class="menu">
    <div id="h-home" class="btn item" onclick="customScrollTo('top')" data-scrolled-passed="true">ទំព័រដើម</div>
    <div id="h-about" class="btn item" onclick="customScrollTo('about')">អំពីយើង</div>
    <div id="h-programs" class="btn item" onclick="customScrollTo('programs')">កម្មវិធី</div>
    <div id="h-contact" class="btn item" onclick="customScrollTo('contact')">ទាក់ទងយើង</div>
    <div id="h-lang" class="btn item lang" onclick="window.location.href = '/'"
      style="background-image: url('/images/client/kh.png')"></div>
  </div>
  <div class="menu-btn" onclick="toggleNavBar()"></div>
</div>
