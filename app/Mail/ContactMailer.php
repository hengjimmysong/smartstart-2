<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMailer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'noreply@smartstart.com';
        $name = 'Notification';
        $subject = 'New Contact Submission from SmartStart Website';

        return $this->view('mailer.contact')
            ->with('contact', $this->contact)
            ->from($address, $name)
            ->subject($subject);
    }
}
