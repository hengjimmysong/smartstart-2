<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('client.main-page.m-home');
});

Route::group(['prefix' => 'hse'], function () {
    Route::get('/', function () {
        return view('client.hse.hse-home');
    });
});

Route::group(['prefix' => 'yip'], function () {
    Route::get('/', function () {
        return view('client.yip.yip-home');
    });
});

Route::group(['prefix' => 'ulp'], function () {
    Route::get('/', function () {
        return view('client.ulp.ulp-home');
    });
});

Route::group(['prefix' => 'kh'], function () {
    Route::get('/', function () {
        return view('client-kh.main-page.m-home');
    });

    Route::group(['prefix' => 'hse'], function () {
        Route::get('/', function () {
            return view('client-kh.hse.hse-home');
        });
    });

    Route::group(['prefix' => 'yip'], function () {
        Route::get('/', function () {
            return view('client-kh.yip.yip-home');
        });
    });

    Route::group(['prefix' => 'ulp'], function () {
        Route::get('/', function () {
            return view('client-kh.ulp.ulp-home');
        });
    });
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });

    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    });
    Route::resources(['users' => 'UserController']);
    Route::resources(['contacts' => 'ContactController']);
});

Route::post('/contacts', 'ContactController@store');


Auth::routes(['register' => false]);
